## Présentation du projet
Le travail à faire consiste à réaliser un système local appelé VaxTodo qui aidera l'organisme à but non lucratif GoodPeople à gérer ses rendez-vous.

Dans ce répertoire se trouve à ce moment-ci un rapport sous format HTML et un prototype de l'application VaxTodo.

## Téléchargement
* Si vous n'avez pas git, voici le lien pour le télécharger: https://git-scm.com/downloads
* Créer un dossier pour héberger le prototype
* Ouvrir le terminal à partir du dossier
* Rentrer la commande suivante: ```git clone https://gabynyn@bitbucket.org/gabynyn/devoirs2255.git```
* Après avoir rentré la dernière commande, tous les fichiers du projet devraient être présent dans le dossier.

## Rapport
Dans ce dossier, vous allez trouver les éléments suivants:

1. Planification (Description du domaine et du problème, échéancier, distribution des tâches, hypothèses)
2. Glossaire
3. Cas d'utilisation
4. Scénarios
5. Analyse (Risques, exigences non-fonctionnelles)
6. Prototypes

## PrototypeVaxTodo
### Description
Dans ce dossier, vous avez accès à tout le code par rapport au prototype.

Tout d'abord, un compte bénévole et employé sont générés aussitôt que l'application roule pour but de tester le prototype.

De plus, un visiteur et des rendez-vous sont aussi générés pour pouvoir tester les fonctionnalités présentées dans l'application.

Version du JDK: openjdk-16

### Fonctionnalités

- Se connecter à l'application
- Consulter le calendrier
- Ajouter une visite (spontanée ou plannifié)
- Consulter la liste des visiteurs
- Créer/Modifier/Supprimer un compte visiteur
- Consulter le profil de vaccination d'un visiteur
- Consulter la liste des bénévoles
- Créer/Modifier/Supprimer un compte bénévole
- Remplir et sauvegarder un formulaire d'identification
- Récupérer et imprimer un formulaire d'identification
- Envoyer un rapport de vaccination
- Envoyer une notification de rappel


### Manuel d'utilisation

Pour utiliser l'application, il vous faut exécuter la commande suivante: `java -jar prototypeVaxTodo` dans le folder où se trouve le fichier jar.
Étant un prototype, nous avons inclus un jeu de données afin de tester l'application.
À l'ouverture, vous devez vous connecter en tant qu'employé ou bénévole. Ceci vous donnera accès
au menu principal propre au rôle.

### Données incluses dans l'application

- Employe("123456789123", "Nguyen", "Gaby", "2000-07-19", "gaby.nguyen@umontreal.ca", "5140000000", "test", "99 avenue Test", "h2k3f9", "Montréal");
- Benevole("123456789000", "tested", "test", "1981-09-20", "test@umontreal.ca", "5141230000", "testb", "100 avenue Test", "w2l3f1", "Montréal");
- Visiteur("000000000000", "Tester", "Test", "1970-10-10", "email@umontreal.ca", "5143450000");
- Questionnaire("000000000000", "Tester", "Test", "1970-10-10", "TEST12345678", "2021-12-15", "NON", "NON", "NON", "NON", "MODERNA","OUI", "PFIZER", "52", "123456");
- RendezVous:
- RendezVous("Nguyen", "Gaby", date, "18", 1, 2);
- RendezVous rdv2 = new RendezVous("Yin", "Wen", dateNext1, "08", 2, 1);
- RendezVous rdv3 = new RendezVous("Zakariya", "Taha", dateNext2, "10", 1, 1);
- RendezVous rdv4 = new RendezVous("Ducoin", "Aurélien", "2021-11-08", "12", 2, 1);
- RendezVous rdv5 = new RendezVous("Lafontant", "Louis-Édouard", "2021-11-08", "18", 1, 2);

### Connexion

Pour se connecter à l'application, veuillez utiliser un des identifiants suivants:

#### Employé
Code identification: ```100000000```

Mot de passe: ```test```

#### Bénévole
Code identification: ```100000001```

Mot de passe: ```testb```

### Menu principal (Employé)

À partir du menu principal, dans le rôle de l'employé, vous pouvez choisir l'une des options suivantes en tapant le chiffre correspondant.

- [1] Consulter la liste liste de rendez-vous: Accéder à la liste de rendez-vous de la journée.
- [2] Consulter le calendrier: Consulter calendrier du lendemain ou à une date cible.
- [3] Gestion des visiteurs: Ajouter, modifier ou supprimer un visiteur.
- [4] Gestion des bénévoles: Ajouter, modifier ou supprimer un bénévole.
- [5] Remplissage questionnaire: Pour remplir questionnaire pour visiteur.
- [6] Liste de visite(s) dans prochain 48h: Pour avoir la liste de visite dans les prochains deux jours.
- [7] Consulter questionnaire d'un visiteur: Avoir le questionnaire d'un visiteur.
- [8] Déconnexion: Pour se déconnecter du système.

#### Gestion des visiteurs

Dans cette section, vous pouvez effectuer les actions suivantes en tapant le chiffre correspondant.
Suivez les instructions à l'écran pour compléter la tache

- [1] Ajouter un visiteur
- [2] Modifier un visiteur
- [3] Supprimer un visiteur
- [4] Ajouter profil de vaccination
- [5] Retour à la page menu

#### Gestion des bénévoles

Dans cette section, vous pouvez effectuer les actions suivantes en tapant le chiffre correspondant.
Suivez les instructions à l'écran pour compléter la tache

- [1] Créer un bénévole
- [2] Modifier un bénévole
- [3] Supprimer un bénévole
- [4] Afficher liste de bénévole
- [5] Retour à la page menu

### Menu principal (Bénévole)

À partir du menu principal, dans le rôle du bénévole, vous pouvez choisir l'une des options suivantes en tapant le chiffre correspondant.

- [1] Consulter la liste liste de rendez-vous: Accéder à la liste de rendez-vous de la journée.
- [2] Consulter le calendrier: Consulter calendrier du lendemain ou à une date cible.
- [3] Déconnexion: Pour se déconnecter du système.