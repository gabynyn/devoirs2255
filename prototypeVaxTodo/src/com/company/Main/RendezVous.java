import java.util.concurrent.atomic.AtomicInteger;

/**
 * Représente un rendez-vous.
 */
public class RendezVous {
    private static final AtomicInteger RESERVATION_GENERATOR = new AtomicInteger(1);
    private String numeroReservation;
    private String nom;
    private String prenom;
    private String dateVisite;
    private String heureVisite;
    private int typeDose;
    private int nbPersonne;

    /**
     * Constructeur RendezVous sans param.
     */
    public RendezVous() {
        this.numeroReservation = null;
        this.nom = null;
        this.prenom = null;
        this.dateVisite = null;
        this.heureVisite = null;
        this.typeDose = 0;
    }

    /**
     * Constructeur RendezVous avec param.
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateVisite date visite
     * @param heureVisite heure visite
     * @param typeDose type dose
     * @param nbPersonne nombre de personnes
     */
    public RendezVous(String nom, String prenom, String dateVisite, String heureVisite, int typeDose, int nbPersonne) {
        this.numeroReservation = Integer.toString(RESERVATION_GENERATOR.getAndIncrement());
        this.nom = nom;
        this.prenom = prenom;
        this.dateVisite = dateVisite;
        this.heureVisite = heureVisite;
        this.typeDose = typeDose;
        this.nbPersonne = nbPersonne;
    }

    /**
     * Méthode pour avoir numéro de réservation.
     * @return numéro de reservation
     */
    public String getNumeroReservation() {
        return numeroReservation;
    }

    /**
     * Méthode pour modifier numéro de réservation.
     * @param numeroReservation nouveau numéro de réservation
     */
    public void setNumeroReservation(String numeroReservation) {
        this.numeroReservation = numeroReservation;
    }

    /**
     * Méthode pour avoir nom.
     * @return nom de la personne
     */
    public String getNom() {
        return nom;
    }

    /**
     * Méthode pour modifier nom.
     * @param nom nouveau nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode pour avoir prénom.
     * @return prénom de la personne
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Méthode pour modifier prenom.
     * @param prenom nouveau prénom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode pour avoir date de visite.
     * @return date de visite
     */
    public String getDateVisite() {
        return dateVisite;
    }

    /**
     * Méthode pour modifier date de visite.
     * @param dateVisite nouvelle date de visite
     */
    public void setDateVisite(String dateVisite) {
        this.dateVisite = dateVisite;
    }

    /**
     * Méthode pour avoir heure de visite.
     * @return heure de visite
     */
    public String getHeureVisite() {
        return heureVisite;
    }

    /**
     * Méthode pour modifier heure de visite.
     * @param heureVisite nouvelle heure de visite
     */
    public void setHeureVisite(String heureVisite) {
        this.heureVisite = heureVisite;
    }

    /**
     * Méthode pour avoir type dose
     * @return type dose
     */
    public int getTypeDose() {
        return typeDose;
    }

    /**
     * Méthode pour modifier type dose
     * @param typeDose nouveau type dose
     */
    public void setTypeDose(int typeDose) {
        this.typeDose = typeDose;
    }

    /**
     * Méthode pour avoir nombre de personne
     * @return nombre de personne
     */
    public int getNbPersonne() {
        return nbPersonne;
    }

    /**
     * Méthode pour modifier nombre de personne
     * @param nbPersonne nouveau nombre de personne
     */
    public void setNbPersonne(int nbPersonne) {
        this.nbPersonne = nbPersonne;
    }
}
