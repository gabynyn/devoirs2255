/**
 * Représente profil de vaccination.
 */
public class ProfilVaccination {
    private String numeroCompte;
    private String dateVaccination;
    private int typeDose;
    private String nomVaccin;
    private String codeVaccin;
    private String lotVaccin;
    private String codeQr;

    /**
     * Constructeur ProfilVaccination sans param.
     */
    public ProfilVaccination() {
        this.numeroCompte = null;
        this.dateVaccination = null;
        this.typeDose = 0;
        this.nomVaccin = null;
        this.codeVaccin = null;
        this.lotVaccin = null;
        this.codeQr = null;
    }

    /**
     * Constructeur ProfilVaccination avec param.
     * @param numeroCompte numéro compte de la personne
     * @param dateVaccination date de vaccination
     * @param typeDose type dose
     * @param nomVaccin nom du vaccin
     * @param codeVaccin code du vaccin
     * @param lotVaccin lot du vaccin
     * @param codeQr code qr généré
     */
    public ProfilVaccination(String numeroCompte, String dateVaccination, int typeDose, String nomVaccin, String codeVaccin, String lotVaccin, String codeQr) {
        this.numeroCompte = numeroCompte;
        this.dateVaccination = dateVaccination;
        this.typeDose = typeDose;
        this.nomVaccin = nomVaccin;
        this.codeVaccin = codeVaccin;
        this.lotVaccin = lotVaccin;
        this.codeQr = codeQr;
    }

    /**
     * Méthode pour avoir numéro de compte.
     * @return numéro de compte
     */
    public String getNumeroCompte() {
        return numeroCompte;
    }

    /**
     * Méthode pour modifier numéro de compte.
     * @param numeroCompte nouveau numéro de compte
     */
    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    /**
     * Méthode pour avoir date de vaccination.
     * @return date de vaccination
     */
    public String getDateVaccination() {
        return dateVaccination;
    }

    /**
     * Méthode pour modifier date de vaccination.
     * @param dateVaccination nouvelle date de vaccination
     */
    public void setDateVaccination(String dateVaccination) {
        this.dateVaccination = dateVaccination;
    }

    /**
     * Méthode pour avoir type dose
     * @return type dose
     */
    public int getTypeDose() {
        return typeDose;
    }

    /**
     * Méthode pour modifier type dose
     * @param typeDose nouveau type dose
     */
    public void setTypeDose(int typeDose) {
        this.typeDose = typeDose;
    }

    /**
     * Méthode pour avoir nom vaccin.
     * @return nom vaccin
     */
    public String getNomVaccin() {
        return nomVaccin;
    }

    /**
     * Méthode pour modifier nom vaccin.
     * @param nomVaccin nouveau nom vaccin
     */
    public void setNomVaccin(String nomVaccin) {
        this.nomVaccin = nomVaccin;
    }

    /**
     * Méthode pour avoir code vaccin.
     * @return code vaccin
     */
    public String getCodeVaccin() {
        return codeVaccin;
    }

    /**
     * Méthode pour modifier code vaccin.
     * @param codeVaccin nouveau code vaccin
     */
    public void setCodeVaccin(String codeVaccin) {
        this.codeVaccin = codeVaccin;
    }

    /**
     * Méthode pour avoir lot vaccin.
     * @return lot vaccin
     */
    public String getLotVaccin() {
        return lotVaccin;
    }

    /**
     * Méthode pour modifier lot vaccin.
     * @param lotVaccin nouveau lot vaccin
     */
    public void setLotVaccin(String lotVaccin) {
        this.lotVaccin = lotVaccin;
    }

    /**
     * Méthode pour avoir code QR.
     * @return code QR
     */
    public String getCodeQr() {
        return codeQr;
    }

    /**
     * Méthode pour modifier code QR.
     * @param codeQr nouveau code QR
     */
    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }
}
