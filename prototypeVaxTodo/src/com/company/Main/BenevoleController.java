import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controlleur qui gère les requêtes reliées au bénévole.
 */
public class BenevoleController {
    Database database;
    Benevole model;
    Menu view;

    /**
     * Constructeur du controlleur
     * @param database database où les données se trouvent
     * @param model données d'un bénévole
     * @param view affichage interface pour utilisateur
     */
    public BenevoleController(Database database, Benevole model, Menu view) {
        this.database = database;
        this.model = model;
        this.view = view;
    }

    /**
     * Méthode de validation des inputs pour supprimer un bénévole.
     * @param codeIdentification code d'identification du bénévole
     * @return true si code d'identification valide, sinon false
     */
    public Boolean validationSupprimerBenevole(String codeIdentification) {
        boolean valid = true;
        CompteUtilisateur user = database.getUser(codeIdentification);
        if (codeIdentification.length() != 9){
            view.promptMessage("Code d'identification n'est pas de 9 chiffres.");
            valid = false;
        }
        if (codeIdentification.length() == 9) {
            if (user == null) {
                view.promptMessage("Le bénévole cherché n'existe pas.");
                valid = false;
            } else if (!user.getType().equals("BENEVOLE")){
                    view.promptMessage("Le code d'identification n'est pas associé à un bénévole.");
                    valid = false;
            }
        }
        return valid;
    }

    /**
     * Méthode de validation des inputs pour ajout d'un bénévole.
     * @param numeroCompte numéro de compte du bénévole
     * @param motDePasse mot de passe du bénévole
     * @param nom nom du bénévole
     * @param prenom prénom du bénévole
     * @param dateNaissance date de naissance du bénévole
     * @param adresse adresse du bénévole
     * @param codePostal code postal du bénévole
     * @param ville ville du bénévole
     * @param adresseCourriel email du bénévole
     * @param numeroTelephone numéro de téléphone du bénévole
     * @return true si inputs valides, sinon false
     */
    public Boolean validationAjoutBenevole(String numeroCompte, String motDePasse, String nom, String prenom, String dateNaissance, String adresse, String codePostal, String ville, String adresseCourriel, String numeroTelephone) {
        boolean valid = true;
        if(!database.getNumeroCompte(numeroCompte)) {
            view.promptMessage("Le numéro de compte est déjà utilisé par quelqu'un d'autre.");
            valid = false;
        }
        if (numeroCompte.length() != 12) {
            view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            valid = false;
        }

        Pattern pattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$");
        Matcher matcher = pattern.matcher(motDePasse);
        if (!matcher.matches()) {
            view.promptMessage("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 caractère spécial.");
            valid = false;
        }

        if(nom.length() > 50) {
            view.promptMessage("Le nom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        if(prenom.length() > 50) {
            view.promptMessage("Le prénom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        try {
            new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
        } catch (ParseException e) {
            view.promptMessage("La date de naissance n'a pas été donnée dans le bon format.");
            valid = false;
        }

        if (adresse.length() > 100) {
            view.promptMessage("L'adresse ne peut pas dépasser 100 caractères.");
            valid = false;
        }

        if (codePostal.length() > 6) {
            view.promptMessage("Le code postal ne peut pas dépasser 6 caractères.");
            valid = false;
        }

        if (ville.length() > 50) {
            view.promptMessage("La ville ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        pattern = Pattern.compile("^(.+)@(.+)$");
        matcher = pattern.matcher(adresseCourriel);
        if (!matcher.matches()) {
            view.promptMessage("L'adresse courriel n'a pas été donnée dans le bon format.");
            valid = false;
        }

        pattern = Pattern.compile("^(\\d{3}[- .]?){2}\\d{4}$");
        matcher = pattern.matcher(numeroTelephone);
        if (!matcher.matches()) {
            view.promptMessage("Le numéro de téléphone n'a pas été donné dans le bon format.");
            valid = false;
        }
        return valid;
    }

    /**
     * Méthode pour ajouter un bénévole.
     * @param benevole bénévole à ajouter
     * @return true si ajouté avec succès, sinon false
     */
    public Boolean ajoutBenevole(Benevole benevole) {
        model = benevole;
        return database.addBenevole(model);
    }

    /**
     * Méthode pour supprimer bénévole.
     * @param codeIdentification code d'identification du bénévole à supprimer
     * @return true si supprimé avec succès, sinon false
     */
    public Boolean supprimerBenevole(String codeIdentification) {
        return database.supprimerBenevole(codeIdentification);
    }

    /**
     * Méthode de validation des inputs pour modification d'un bénévole.
     * @param codeIdentification code d'identification du bénévole
     * @param motDePasse mot de passe du bénévole
     * @param nom nom du bénévole
     * @param prenom prénom du bénévole
     * @param dateNaissance date de naissance du bénévole
     * @param adresse adresse du bénévole
     * @param codePostal code postal du bénévole
     * @param ville ville du bénévole
     * @param adresseCourriel email du bénévole
     * @param numeroTelephone numéro de téléphone du bénévole
     * @return true si inputs valides, sinon false
     */
    public Boolean validationModifierBenevole(String codeIdentification, String motDePasse, String nom, String prenom, String dateNaissance, String adresse, String codePostal, String ville, String adresseCourriel, String numeroTelephone) {
        boolean valid = true;
        if(database.getBenevole(codeIdentification) == null) {
            view.promptMessage("Le numéro de compte n'est pas associé à un compte.");
            valid = false;
            if (codeIdentification.length() != 12) {
                view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            }
        } else {
            Pattern pattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$");
            Matcher matcher = pattern.matcher(motDePasse);
            if (!motDePasse.isEmpty() && !motDePasse.isBlank()) {
                if (!matcher.matches()) {
                    view.promptMessage("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 caractère spécial.");
                    valid = false;
                }
            }

            if (!nom.isEmpty() && !nom.isBlank()) {
                if (nom.length() > 50) {
                    view.promptMessage("Le nom ne peut pas dépasser 50 caractères.");
                    valid = false;
                }
            }

            if (!prenom.isEmpty() && !prenom.isBlank()) {
                if (prenom.length() > 50) {
                    view.promptMessage("Le prénom ne peut pas dépasser 50 caractères.");
                    valid = false;
                }
            }

            if (!dateNaissance.isEmpty() && !dateNaissance.isBlank()) {
                try {
                    new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
                } catch (ParseException e) {
                    view.promptMessage("La date de naissance n'a pas été donnée dans le bon format.");
                    valid = false;
                }
            }

            if (!adresse.isEmpty() && !adresse.isBlank()) {
                if (adresse.length() > 100) {
                    view.promptMessage("L'adresse ne peut pas dépasser 100 caractères.");
                    valid = false;
                }
            }

            if (!codePostal.isEmpty() && !codePostal.isBlank()) {
                if (codePostal.length() > 6) {
                    view.promptMessage("Le code postal ne peut pas dépasser 6 caractères.");
                    valid = false;
                }
            }

            if (!ville.isEmpty() && !ville.isBlank()) {
                if (ville.length() > 50) {
                    view.promptMessage("La ville ne peut pas dépasser 50 caractères.");
                    valid = false;
                }
            }

            pattern = Pattern.compile("^(.+)@(.+)$");
            matcher = pattern.matcher(adresseCourriel);
            if (!adresseCourriel.isEmpty() && !adresseCourriel.isBlank()) {
                if (!matcher.matches()) {
                    view.promptMessage("L'adresse courriel n'a pas été donnée dans le bon format.");
                    valid = false;
                }
            }

            pattern = Pattern.compile("^(\\d{3}[- .]?){2}\\d{4}$");
            matcher = pattern.matcher(numeroTelephone);
            if (!numeroTelephone.isEmpty() && !numeroTelephone.isBlank()) {
                if (!matcher.matches()) {
                    view.promptMessage("Le numéro de téléphone n'a pas été donné dans le bon format.");
                    valid = false;
                }
            }
        }
        return valid;
    }

    /**
     * Méthode pour modifier information d'un bénévole.
     * @param codeIdentification code d'identification du bénévole
     * @param motDePasse mot de passe du bénévole
     * @param nom nom du bénévole
     * @param prenom prénom du bénévole
     * @param dateNaissance date de naissance du bénévole
     * @param adresse adresse du bénévole
     * @param codePostal code postal du bénévole
     * @param ville ville du bénévole
     * @param adresseCourriel email du bénévole
     * @param numeroTelephone numéro de téléphone du bénévole
     * @return true si modifié avec succès, sinon false
     * @throws ParseException SimpleDateFormat peut causer un parse exception
     */
    public Boolean modifierBenevole(String codeIdentification, String motDePasse, String nom, String prenom, String dateNaissance, String adresse, String codePostal, String ville, String adresseCourriel, String numeroTelephone) throws ParseException {
        Benevole cible = database.getBenevole(codeIdentification);

        if (!motDePasse.isEmpty() && !motDePasse.isBlank()) {
            cible.setMotDePasse(motDePasse);
        }
        if (!nom.isEmpty() && !nom.isBlank()) {
            cible.setNom(nom);
        }
        if (!prenom.isEmpty() && !prenom.isBlank()) {
            cible.setPrenom(prenom);
        }
        if (!dateNaissance.isEmpty() && !dateNaissance.isBlank()) {
            cible.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance));
        }
        if (!adresse.isEmpty() && !adresse.isBlank()) {
            cible.setAdresse(adresse);
        }
        if (!codePostal.isEmpty() && !codePostal.isBlank()) {
            cible.setCodePostal(codePostal);
        }
        if (!ville.isEmpty() && !ville.isBlank()) {
            cible.setVille(ville);
        }
        if (!adresseCourriel.isEmpty() && !adresseCourriel.isBlank()) {
            cible.setAdresseCourriel(adresseCourriel);
        }
        if (!numeroTelephone.isEmpty() && !numeroTelephone.isBlank()) {
            cible.setNumeroTelephone(numeroTelephone);
        }
        return database.updateBenevole(cible);
    }

    /**
     * Méthode pour avoir la liste de bénévole
     */
    public void getListeBenevole() {
        ArrayList<Benevole> liste = database.getListeBenevole();
        if (liste.isEmpty()) {
            view.promptMessage("Il n'y a aucun bénévole.");
        } else {
            view.afficherListeBenevole(liste);
        }
    }
}
