import java.text.ParseException;

/**
 * Représente employé.
 */
public class Employe extends CompteUtilisateur{

    /**
     * Constructeur avec param.
     * @param numeroCompte numéro compte utilisateur
     * @param nom nom utilisateur
     * @param prenom prénom utilisateur
     * @param dateNaissance date de naissance utilisateur
     * @param adresseCourriel adresse courriel utilisateur
     * @param numeroTelephone numéro de téléphone utilisateur
     * @param motDePasse mot de passe utilisateur
     * @param adresse adresse utilisateur
     * @param codePostal code postal utilisateur
     * @param ville ville utilisateur
     * @throws ParseException au cas qu'il y a un problème de parse
     */
    public Employe(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone, String motDePasse, String adresse, String codePostal, String ville) throws ParseException {
        super(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone, motDePasse, adresse, codePostal, ville, 1);

    }
}
