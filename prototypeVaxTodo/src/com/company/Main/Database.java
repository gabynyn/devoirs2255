import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Base de donnée du système.
 */
public class Database {
    private final HashMap<String, CompteUtilisateur> listeUtilisateur = new HashMap<>();
    private final LinkedList<RendezVous> listeRendezVous = new LinkedList<>();
    private final HashMap<String, Visiteur> listeVisiteur = new HashMap<>();
    private final LinkedList<String> numeroCompteUtilise = new LinkedList<>();
    private final LinkedList<Questionnaire> listeQuestionnaire = new LinkedList<>();

    /**
     * Méthode pour ajouter utilisateur.
     * @param user utilisateur à ajouter
     */
    public void addUser(CompteUtilisateur user) {
        listeUtilisateur.put(user.getCodeIdentification(), user);
        numeroCompteUtilise.add(user.getNumeroCompte());
    }

    /**
     * Méthode pour avoir utilisateur.
     * @param codeIdentification code identification utilisateur
     * @return utilisateur cible
     */
    public CompteUtilisateur getUser(String codeIdentification) {
        return listeUtilisateur.get(codeIdentification);
    }

    /**
     * Méthode pour ajouter rendez-vous.
     * @param rendezVous rendez-vous à ajouter
     * @return true si ajouter avec succès, sinon false
     */
    public Boolean addRendezVous(RendezVous rendezVous) {
        listeRendezVous.add(rendezVous);
        for (RendezVous rdv : listeRendezVous) {
            if(rdv == rendezVous) {
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode pour avoir la liste de rendez-vous de la journée.
     * @return liste rendez-vous de la journée.
     */
    public ArrayList<RendezVous> getListeRendezVous() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date now = Calendar.getInstance().getTime();
        String dateVisite = dateFormat.format(now);
        ArrayList<RendezVous> liste = new ArrayList<>();
        for (RendezVous rdv : listeRendezVous) {
            if(rdv.getDateVisite().equals(dateVisite)) {
                liste.add(rdv);
            }
        }
        return liste;
    }

    /**
     * Méthode pour avoir les plages d'horaire d'une date.
     * @param date date cible
     * @return liste avec plage horaire de la date cible
     */
    public HashMap<String, Integer> getPlagesHoraire(String date) {
        HashMap<String ,Integer> liste = new HashMap<>();

        for(int i = 8; i <= 18; i++) {
            String time;
            int nbPersonne = 0;
            if(i < 10) {
                time = "0" + i;
            } else {
                time = String.valueOf(i);
            }
            for (RendezVous rdv : listeRendezVous) {
                if (rdv.getHeureVisite().equals(time) && rdv.getDateVisite().equals(date)) {
                    nbPersonne += rdv.getNbPersonne();
                }
                liste.put(time, nbPersonne);
            }
        }
        return liste;
    }

    /**
     * Méthode pour avoir visiteur.
     * @param numeroCompte numéro compte du visiteur
     * @return visiteur cherché
     */
    public Visiteur getVisiteur(String numeroCompte) {
        return listeVisiteur.get(numeroCompte);
    }

    /**
     * Méthode pour ajouter visiteur.
     * @param visiteur visiteur à ajouter
     * @return true si ajouté avec succès, sinon false
     */
    public Boolean addVisiteur(Visiteur visiteur) {
        listeVisiteur.put(visiteur.getNumeroCompte(), visiteur);
        numeroCompteUtilise.add(visiteur.getNumeroCompte());
        return listeVisiteur.get(visiteur.getNumeroCompte()) != null;
    }

    /**
     * Méthode pour supprimer visiteur.
     * @param numeroCompte numéro compte du visiteur
     * @return true si supprimé avec succès, sinon false
     */
    public Boolean supprimerVisiteur(String numeroCompte) {
        listeVisiteur.remove(numeroCompte);
        return getVisiteur(numeroCompte) == null;
    }

    /**
     * Méthode pour modifier visiteur.
     * @param visiteur visiteur modifié
     * @return true si modifié avec succès, sinon false
     */
    public Boolean updateVisiteur(Visiteur visiteur) {
        return listeVisiteur.get(visiteur.getNumeroCompte()) == visiteur;
    }

    /**
     * Méthode pour avoir bénévole.
     * @param codeIdentification code identification bénévole.
     * @return benevole cible
     */
    public Benevole getBenevole(String codeIdentification) {
        return (Benevole) listeUtilisateur.get(codeIdentification);
    }

    /**
     * Méthode pour ajouter bénévole.
     * @param benevole bénévole à ajouter
     * @return true si bénévole ajouté avec succès, sinon false
     */
    public Boolean addBenevole(Benevole benevole) {
        listeUtilisateur.put(benevole.getCodeIdentification(), benevole);
        numeroCompteUtilise.add(benevole.getNumeroCompte());
        return listeUtilisateur.get(benevole.getCodeIdentification()) != null;
    }

    /**
     * Méthode pour supprimer bénévole.
     * @param codeIdentification code identification bénévole.
     * @return true si bénévole supprimé avec succès, sinon false
     */
    public Boolean supprimerBenevole(String codeIdentification) {
        listeUtilisateur.remove(codeIdentification);
        return getBenevole(codeIdentification) == null;
    }

    /**
     * Méthode pour modifier bénévole.
     * @param benevole bénévole modifié
     * @return true si bénévole modifié avec succès, sinon false
     */
    public Boolean updateBenevole(Benevole benevole) {
        return listeUtilisateur.get(benevole.getCodeIdentification()) == benevole;
    }

    /**
     * Méthode pour avoir liste de bénévole(s).
     * @return liste de bénévole(s)
     */
    public ArrayList<Benevole> getListeBenevole() {
        ArrayList<Benevole> liste = new ArrayList<>();
        for (Map.Entry<String, CompteUtilisateur> entry : listeUtilisateur.entrySet()) {
            CompteUtilisateur value = entry.getValue();
            if(value.getType().equals("BENEVOLE")) {
                liste.add((Benevole) value);
            }
        }
        return liste;
    }

    /**
     * Méthode pour vérifier si numéro de compte utilisé.
     * @param numeroCompte numéro de compte à vérifier
     * @return true si pas utilisé, sinon false
     */
    public Boolean getNumeroCompte(String numeroCompte) {
        for (String s : numeroCompteUtilise) {
            if (numeroCompte.equals(s)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Méthode pour avoir la liste des visites dans prochain 48 heures.
     * @return liste des prochaines visites
     */
    public ArrayList<RendezVous> getProchaineVisite() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date now = Calendar.getInstance().getTime();
        ArrayList<RendezVous> liste = new ArrayList<>();

        cal.setTime(now);
        for (int i = 1; i <= 2; i++) {
            cal.add(Calendar.DATE, 1);
            Date dateProchain = cal.getTime();
            String dateVisite = dateFormat.format(dateProchain);

            for (RendezVous rdv : listeRendezVous) {
                if(rdv.getDateVisite().equals(dateVisite)) {
                    liste.add(rdv);
                }
            }
        }
        return liste;
    }

    /**
     * Méthode pour avoir questionnaire.
     * @param numeroCompte numéro compte visiteur
     * @return liste contenant questionnaire(s)
     */
    public LinkedList<Questionnaire> getQuestionnaire(String numeroCompte) {
        LinkedList<Questionnaire> liste = new LinkedList<>();
        for (Questionnaire questionnaire : listeQuestionnaire) {
            if(questionnaire.getNumeroCompte().equals(numeroCompte)) {
                liste.add(questionnaire);
            }
        }
        return liste;
    }

    /**
     * Méthode ajout questionnaire.
     * @param questionnaire questionnaire à ajouter
     * @return true si questionnaire ajouté avec succès, sinon false
     */
    public Boolean addQuestionnaire(Questionnaire questionnaire) {
        listeQuestionnaire.add(questionnaire);
        for (Questionnaire q : listeQuestionnaire) {
            if (q.equals(questionnaire)) {
                return true;
            }
        }
        return false;
    }
}
