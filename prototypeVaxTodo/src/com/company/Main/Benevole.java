import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Représente un bénévole
 */
public class Benevole extends CompteUtilisateur {

    ArrayList<Date> disponibilites;

    /**
     * Constructeur bénévole sans param
     * @throws ParseException SimpleDateFormat peut causer un parse exception
     */
    public Benevole() throws ParseException {
        super(null, null, null, "0000-00-00", null, null, null, null, null, null, 0);
        disponibilites = null;
    }

    /**
     * Constructeur de bénévole.
     * @param numeroCompte numéro du compte du bénévole
     * @param nom nom du bénévole
     * @param prenom prénom du bénévole
     * @param dateNaissance date de naissance du bénévole
     * @param adresseCourriel email du bénévole
     * @param numeroTelephone numéro de téléphone du bénévole
     * @param motDePasse mot de passe du compte du bénévole
     * @param adresse adresse du bénévole
     * @param codePostal code postal du bénévole
     * @param ville ville du bénévole
     * @throws ParseException SimpleDateFormat peut causer un parse exception
     */
    public Benevole(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone, String motDePasse, String adresse, String codePostal, String ville) throws ParseException {
        super(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone, motDePasse, adresse, codePostal, ville, 0);
        disponibilites = null;
    }
}
