import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
/**
 * Controlleur qui gère les requêtes reliées au rendez-vous.
 */
public class RendezVousController {

    private Database database;
    private RendezVous model;
    private Menu view;

    /**
     * Constructeur RendezVous.
     * @param database database où les données se trouvent
     * @param model données d'un rendez-vous
     * @param view affichage interface pour utilisateur
     */
    public RendezVousController(Database database, RendezVous model, Menu view) {
        this.database = database;
        this.model = model;
        this.view = view;
    }

    /**
     * Méthode pour avoir liste de rendez-vous de la journée.
     * @return liste de rendez-vous de la journée
     */
    public ArrayList<RendezVous> getListeRendezVous() {
        ArrayList<RendezVous> liste = database.getListeRendezVous();
        if (liste.size() > 0) {
            view.afficherListeRendezVous(liste);
        } else {
            view.promptMessage("Il n'y a aucun rendez-vous aujourd'hui. Retour à la page menu.");
        }
        return liste;
    }

    /**
     * Méthode pour avoir calendrier de la prochaine journée.
     * @param nbPersonne nombre de personne
     */
    public void getCalendrier(int nbPersonne) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date now = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        String dateCible = dateFormat.format(now);
        HashMap<String, Integer> liste = database.getPlagesHoraire(dateCible);
        Date date;
        boolean libre = false;
        if (liste.size() != 0) {
            do {
                for(int i = 8; i <= 18; i++) {
                    String time;
                    int capacity;
                    if (i < 10) {
                        time = "0" + i;
                    } else {
                        time = String.valueOf(i);
                    }
                    capacity = liste.get(time);
                    if(capacity != 0) {
                        libre = capacity + nbPersonne <= 40;
                    }
                    else {
                        libre = true;
                    }
                    if(!libre) {
                        view.promptMessage("Aucune disponibilité à la date " + dateCible);
                        calendar.add(Calendar.DATE, 1);
                        date = calendar.getTime();
                        dateCible = dateFormat.format(date);
                        liste = database.getPlagesHoraire(dateCible);
                    }
                }

            } while (!libre && liste.size() != 0);
        }
        view.afficherCalendrier(liste);
    }

    /**
     * Méthode pour avoir calendrier d'une date cible.
     * @param nbPersonne nombre de personne
     * @param dateVisite date cible
     * @throws ParseException dateFormat.parse peut throw un ParseException
     */
    public void getCalendrier(int nbPersonne, String dateVisite) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(dateVisite));
        String dateCible = dateVisite;
        HashMap<String, Integer> liste = database.getPlagesHoraire(dateCible);
        boolean libre = false;
        Date date;
        if (liste.size() != 0) {
            do {
                for(int i = 8; i <= 18; i++) {
                    String time;
                    int capacity;
                    if (i < 10) {
                        time = "0" + i;
                    } else {
                        time = String.valueOf(i);
                    }
                    capacity = liste.get(time);
                    if(capacity != 0) {
                        libre = capacity + nbPersonne <= 40;
                    }
                    else {
                        libre = true;
                    }
                }
                if(!libre) {
                    view.promptMessage("Aucune disponibilité à la date " + dateVisite);
                    calendar.add(Calendar.DATE, 1);
                    date = calendar.getTime();
                    dateCible = dateFormat.format(date);
                    liste = database.getPlagesHoraire(dateCible);
                }
            } while (!libre && liste.size() != 0);
        }
        view.afficherCalendrier(liste);
    }

    /**
     * Méthode pour valider inputs pour rendez-vous.
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateVisite date visite
     * @param heureVisite heure visite
     * @param typeDose type dose
     * @param nbPersonne nombre de personne
     * @return true si inputs valides, sinon false
     */
    public Boolean validationRendezVous(String nom, String prenom, String dateVisite, String heureVisite, int typeDose, int nbPersonne) {
        boolean valid = true;

        if(nom.length() > 50) {
            view.promptMessage("Le nom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        if(prenom.length() > 50) {
            view.promptMessage("Le prénom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        String patternDate = "yyyy-MM-dd";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 3);
        Date now = Calendar.getInstance().getTime();
        Date after = calendar.getTime();
        try {
            Date date = new SimpleDateFormat(patternDate).parse(dateVisite);
            String dateToString = new SimpleDateFormat(patternDate).format(now);
            if (date.before(now)) {
                if (!dateToString.equals(dateVisite)) {
                    view.promptMessage("La date de visite n'est pas valide.");
                    valid = false;
                }
            }
            if (date.after(after)) {
                view.promptMessage("La date de visite n'est pas valide.");
                valid = false;
            }
        } catch (ParseException e) {
            view.promptMessage("La date de visite n'a pas été donnée dans le bon format.");
            valid = false;
        }

        String patternTime = "HH";
        try {
            if(Integer.parseInt(heureVisite) < 10) {
                heureVisite = "0" + heureVisite;
            }
            new SimpleDateFormat(patternTime).parse(heureVisite);
            if(Integer.parseInt(heureVisite) < 8 || Integer.parseInt(heureVisite) > 18) {
                view.promptMessage("L'heure de visite n'est pas durant une heure d'ouverture.");
                valid = false;
            }
        } catch (Exception e) {
            view.promptMessage("L'heure de visite n'a pas été donnée dans le bon format.");
            valid = false;
        }

        if (typeDose < 1 || typeDose > 2) {
            view.promptMessage("Le type de dose est invalide.");
            valid = false;
        }

        if (nbPersonne < 1 || nbPersonne > 2) {
            view.promptMessage("Le nombre minimum de personne est de 1 et le nombre maximum est de 2.");
            valid = false;
        }
        return valid;
    }

    /**
     * Méthode pour valider disponibilité.
     * @param dateVisite date cible
     * @param heureVisite heure cible
     * @param nbPersonne nombre de personne dans rendez-vous
     * @return true si disponible, sinon false
     */
    public Boolean validationDispo(String dateVisite, String heureVisite, int nbPersonne){
        HashMap<String, Integer> plagesHoraire = database.getPlagesHoraire(dateVisite);
        int capacite;
        if (plagesHoraire.get(heureVisite) == null) {
            capacite = 0;
        } else {
           capacite = plagesHoraire.get(heureVisite);
        }
        if (capacite + nbPersonne > 30) {
            view.promptMessage("Capacité maximale dépassée dans cette plage.");
            return false;
        }
        return true;
    }

    /**
     * Méthode pour ajouter rendez-vous.
     * @param rdv nouveau rendez-vous
     * @return true si ajouter avec succès, sinon false
     */
    public Boolean ajouterRendezVous(RendezVous rdv) {
        if (rdv != null) {
            model = rdv;
        } else {
            return false;
        }
        return database.addRendezVous(model);
    }

    /**
     * Méthode pour afficher les visites dans prochains 48 heures.
     */
    public void getProchaineVisite() {
        ArrayList<RendezVous> liste = database.getProchaineVisite();
        if (liste.size() > 0) {
            view.afficherProchainRendezVous(liste);
            sendNotification(liste);
        } else {
            view.promptMessage("Il n'y a aucun rendez-vous dans les prochains jours. Retour à la page menu.");
        }
        view.promptMessage("Peser sur [ENTER] pour retourner à la page menu.");
    }

    /**
     * Méthode pour envoyer courriels
     * @param liste liste de personne à qui envoyer un courriel
     */
    public void sendNotification(ArrayList<RendezVous> liste) {
        view.promptMessage("Des courriels pour notifier les visiteurs en question ont été envoyés.");
    }
}
