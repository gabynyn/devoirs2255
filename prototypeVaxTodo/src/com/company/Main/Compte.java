import java.util.*;

/**
 * Représente un compte.
 */
public abstract class Compte {
    /*
        numéroCompte: Numéro du compte
        nom: Nom de famille de personne
        prenom: Prenom de la personne
        dateNaissance: Date de naissance de la personne
        adresseCourriel: Adresse courriel de la personne
        numeroTelephone: Numéro de téléphone de la personne
     */
    private String numeroCompte;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String adresseCourriel;
    private String numeroTelephone;

    /**
     * Constructeur d'un compte.
     * @param numeroCompte numéro du compte
     * @param nom nom de l'utilisateur
     * @param prenom prénom de l'utilisateur
     * @param dateNaissance date de naissance de l'utilisateur
     * @param adresseCourriel email de l'utilisateur
     * @param numeroTelephone numéro de téléphone de l'utilisateur
     */
    public Compte(String numeroCompte, String nom, String prenom, Date dateNaissance, String adresseCourriel, String numeroTelephone) {
        this.numeroCompte = numeroCompte;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.adresseCourriel = adresseCourriel;
        this.numeroTelephone = numeroTelephone;
    }

    /**
     * Méthode pour avoir numeroCompte.
     * @return numéro du compte
     */
    public String getNumeroCompte() {
        return numeroCompte;
    }

    /**
     * Méthode pour modifier numeroCompte.
     * @param numeroCompte nouveau numéro de compte
      */
    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    /**
     * Méthode pour avoir nom.
     * @return nom de la personne
     */
    public String getNom() {
        return nom;
    }

    /**
     * Méthode pour modifier nom.
     * @param nom nouveau nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode pour avoir prénom.
     * @return prénom de la personne
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Méthode pour modifier prenom.
     * @param prenom nouveau prénom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode pour avoir la date de naissance.
     * @return date de naissance de la personne
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Méthode pour modifier la date de naissance.
     * @param dateNaissance nouvelle date de naissance
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Méthode pour avoir adresse courriel.
     * @return adresse courriel de la personne
     */
    public String getAdresseCourriel() {
        return adresseCourriel;
    }

    /**
     * Méthode pour modifier l'adresse courriel.
     * @param adresseCourriel nouvelle adresse courriel
     */
    public void setAdresseCourriel(String adresseCourriel) {
        this.adresseCourriel = adresseCourriel;
    }

    /**
     * Méthode pour avoir numéro de téléphone.
     * @return numéro de téléphone de la personne
     */
    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    /**
     * Méthode pour modifier numéro de téléphone.
     * @param numeroTelephone nouveau numéro de téléphone
     */
    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }
}
