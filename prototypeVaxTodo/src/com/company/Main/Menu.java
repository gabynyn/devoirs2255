import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Représente interface que utilisateur voit.
 */
public class Menu {

    private final Scanner scanner = new Scanner(System.in);
    private CompteUtilisateur utilisateurConnecte;
    private int maxOption = 0;

    /**
     * Constructeur de Menu.
     * @param utilisateurConnecte utilisateur connecté.
     */
    public Menu(CompteUtilisateur utilisateurConnecte) {
        this.utilisateurConnecte = utilisateurConnecte;
    }

    /**
     * Méthode pour afficher page au départ du démarrage de l'application.
     */
    public void start() {
        clearScreen();
        System.out.println("BIENVENUE SUR VAXTODO");
        System.out.println("Veuillez choisir l'une des options suivantes:");
        System.out.println("1. Se connecter");
        System.out.println("2. Fermer application");
        System.out.println("______________________________________________");
        maxOption = 2;
    }

    /**
     * Méthode pour affichage page lorsque ferme application
     */
    public void exit() {
        System.out.println("Merci d'avoir utilisé VaxTodo.");
    }

    /**
     * Méthode pour afficher page login.
     */
    public void afficherLogin() {
        clearScreen();
        System.out.println("Entrez votre code d'identification:");
        String codeIdentification = scanner.nextLine();
        System.out.println("Entrez votre mot de passe:");
        String motDePasse = scanner.nextLine();
        utilisateurConnecte.setCodeIdentification(codeIdentification);
        utilisateurConnecte.setMotDePasse(motDePasse);
    }

    /**
     * Méthode pour afficher page qui demande date de calendrier cible.
     */
    public void requestCalendrierDate() {
        System.out.println("Veuillez rentrer la date cible (format: yyyy-MM-dd):");
    }

    /**
     * Méthode pour afficher page menu bénévole.
     */
    public void afficherPageBenevole() {
        clearScreen();
        System.out.println("VAXTODO");
        System.out.println("Bienvenue sur la page de bénévole. Veuillez choisir parmi les options suivantes:");
        System.out.println("1. Consulter la liste de rendez-vous");
        System.out.println("2. Consulter le calendrier");
        System.out.println("3. Déconnexion");
        System.out.println("______________________________________________");
        maxOption = 3;
    }

    /**
     * Méthode pour afficher page menu employé.
     */
    public void afficherPageEmploye() {
        clearScreen();
        System.out.println("VAXTODO");
        System.out.println("Bienvenue sur la page d'employé. Veuillez choisir parmi les options suivantes:");
        System.out.println("1. Consulter la liste de rendez-vous");
        System.out.println("2. Consulter le calendrier");
        System.out.println("3. Gestion des visiteurs");
        System.out.println("4. Gestion des bénévoles");
        System.out.println("5. Remplissage questionnaire");
        System.out.println("6. Liste de visite(s) dans prochain 48h");
        System.out.println("7. Consulter questionnaire d'un visiteur");
        System.out.println("8. Déconnexion");
        System.out.println("______________________________________________");
        maxOption = 8;
    }

    /**
     * Méthode pour afficher page liste de rendez-vous de la journée.
     * @param liste liste de rendez-vous de la journée.
     */
    public void afficherListeRendezVous(ArrayList<RendezVous> liste) {
        clearScreen();
        for (RendezVous rdv : liste) {
            System.out.printf("%1$-1s %2$-10s %3$-1s %4$-10s %5$-1s %6$-15s %7$-1s %8$-20s %9$-1s %10$-15d", "Numéro de réservation: ", rdv.getNumeroReservation(), "Heure de visite: ", rdv.getHeureVisite()+":00", "Nom: ", rdv.getNom(), "Prénom: ", rdv.getPrenom(), "Type dose: ", rdv.getTypeDose());
            System.out.println();
        }
        System.out.println("Peser sur [ENTER] pour retourner à la page menu.");
    }

    /**
     * Méthode pour afficher page calendrier d'une date.
     * @param liste plage horaire de la date.
     */
    public void afficherCalendrier(HashMap<String, Integer> liste) {
        clearScreen();
        if (liste.size() != 0) {
            for(int i = 8; i <= 18; i++) {
                String time;
                if (i < 10) {
                    time = "0" + i;
                } else {
                    time = String.valueOf(i);
                }
                System.out.printf("%1$-1s %2$-10s", "Heure visite: ", time+":00");
                if(liste.get(time) == null) {
                    System.out.print("Nombre de personne(s): " + "0");
                } else {
                    System.out.print("Nombre de personne(s): " + liste.get(time));
                }
                System.out.println();
            }
        } else {
            System.out.println("Toute la journée est disponible.");
        }
    }

    /**
     * Méthode pour afficher page formulaire rendez-vous.
     */
    public void requestRendezVousInfos(){
        clearScreen();
        System.out.println("1. Rentrer le nom du visiteur:");
        System.out.println("2. Rentrer le prénom du visiteur:");
        System.out.println("3. Rentrer la date de visite sous le format [YYYY-MM-DD]:");
        System.out.println("4. Rentrer l'heure de la visite sous le format de [HH]:");
        System.out.println("5. Rentrer le type de dose:");
        System.out.println("6. Rentrer le nombre de personnes (Min 1, Max 2):");
        System.out.println("__________________________________________________________");
    }

    /**
     * Méthode pour afficher page demande ajout rendez-vous.
     */
    public void requestAjoutRendezVous() {
        clearScreen();
        System.out.println("Voulez vous ajouter un rendez-vous?");
        System.out.println("1. Oui");
        System.out.println("2. Non, retour à la page menu");
        maxOption = 2;
    }

    /**
     * Méthode pour afficher page demandant nombre de personne dans local.
     */
    public void requestCalendrierNbPersonne() {
        clearScreen();
        System.out.println("Veuillez rentrer le nombre de personne présent dans le local:");
    }

    /**
     * Méthode pour page affichant option calendrier.
     */
    public void requestCalendrierOption() {
        clearScreen();
        System.out.println("1. Afficher calendrier lendemain");
        System.out.println("2. Choisir date dont vous voulez le calendrier");
        System.out.println("Veuillez choisir l'une des options.");
        maxOption = 2;
    }

    /**
     * Méthode pour page options gestion visiteur.
     */
    public void requestVisiteurOption() {
        clearScreen();
        System.out.println("Veuillez choisir l'une des options suivantes:");
        System.out.println("1. Ajouter un visiteur");
        System.out.println("2. Supprimer un visiteur");
        System.out.println("3. Modifier un visiteur");
        System.out.println("4. Ajouter profil de vaccination");
        System.out.println("5. Retour à la page menu");
        System.out.println("__________________________________________________________");
        maxOption = 5;
    }

    /**
     * Méthode pour page cueillette information visiteur.
     */
    public void requestVisiteurInfos() {
        clearScreen();
        System.out.println("1. Rentrer le numero du compte visiteur:");
        System.out.println("2. Rentrer le nom du visiteur:");
        System.out.println("3. Rentrer le prénom du visiteur:");
        System.out.println("4. Rentrer la date de naissance du visiteur dans le format [YYYY-MM-DD]:");
        System.out.println("5. Rentrer l'adresse courriel du visiteur:");
        System.out.println("6. Rentrer le numéro de téléphone du visiteur:");
    }

    /**
     * Méthode pour page cueillette information pour supprimer visiteur.
     */
    public void requestSupprimerVisiteurInfos() {
        clearScreen();
        System.out.println("Svp rentrer le numero de compte du visiteur que vous voulez supprimer:");
    }

    /**
     * Méthode pour affichage page demande informations profil vaccination.
     */
    public void requestProfilVaccinationInfos() {
        clearScreen();
        System.out.println("1. Rentrer le numéro de compte du visiteur: ");
        System.out.println("2. Rentrer la date de vaccination sous le format [YYYY-MM-DD]: ");
        System.out.println("3. Rentrer le type de dose: ");
        System.out.println("4. Rentrer le nom du vaccin [Moderna, Pfizer, AstraZeneca, Janssen]: ");
        System.out.println("5. Rentrer le code du vaccin (24 caractères maximum): ");
        System.out.println("6. Rentrer le lot du vaccin (6 caractères maximum): ");
    }

    /**
     * Méthode pour affichage page option gestion bénévole.
     */
    public void requestBenevoleOption() {
        clearScreen();
        System.out.println("Veuillez choisir l'une des options suivantes:");
        System.out.println("1. Ajouter un bénévole");
        System.out.println("2. Supprimer un bénévole");
        System.out.println("3. Modifier un bénévole");
        System.out.println("4. Afficher liste de bénévole");
        System.out.println("5. Retour à la page menu");
        System.out.println("__________________________________________________________");
        maxOption = 5;
    }

    /**
     * Méthode pour affichage page demande information pour ajout bénévole.
     */
    public void requestAjoutBenevoleInfos() {
        clearScreen();
        System.out.println("1. Rentrer le numero du compte bénévole: ");
        System.out.println("2. Rentrer le mot de passe du bénévole [Minimum: 1 minuscule, 1 majuscule, 1 caractère spécial]: ");
        System.out.println("3. Rentrer le nom du bénévole: ");
        System.out.println("4. Rentrer le prénom du bénévole: ");
        System.out.println("5. Rentrer la date de naissance du bénévole dans le format [YYYY-MM-DD]: ");
        System.out.println("6. Rentrer l'adresse du bénévole: ");
        System.out.println("7. Rentrer le code postal du bénévole: ");
        System.out.println("8. Rentrer la ville où se situe le bénévole: ");
        System.out.println("9. Rentrer l'adresse courriel du bénévole: ");
        System.out.println("10. Rentrer le numéro de téléphone du bénévole: ");
    }

    /**
     * Méthode pour affichage page demande information pour modifier bénévole.
     */
    public void requestModifierBenevoleInfos() {
        clearScreen();
        System.out.println("Veuillez laisser vide les informations dont vous ne modifié pas sauf pour le numéro du compte.");
        System.out.println("1. Rentrer le code d'identification du bénévole: ");
        System.out.println("2. Rentrer le mot de passe du bénévole [Minimum: 1 minuscule, 1 majuscule, 1 caractère spécial]: ");
        System.out.println("3. Rentrer le nom du bénévole: ");
        System.out.println("4. Rentrer le prénom du bénévole: ");
        System.out.println("5. Rentrer la date de naissance du bénévole dans le format [YYYY-MM-DD]: ");
        System.out.println("6. Rentrer l'adresse du bénévole: ");
        System.out.println("7. Rentrer le code postal du bénévole: ");
        System.out.println("8. Rentrer la ville où se situe le bénévole: ");
        System.out.println("9. Rentrer l'adresse courriel du bénévole: ");
        System.out.println("10. Rentrer le numéro de téléphone du bénévole: ");
    }

    /**
     * Méthode pour affichage page demande information pour supprimer bénévole.
     */
    public void requestSupprimerBenevoleInfos() {
        clearScreen();
        System.out.println("Svp rentrer le numero d'identification du benevole que vous voulez supprimer:");
    }

    /**
     * Méthode pour affichage page demande information pour questionnaire.
     */
    public void requestQuestionnaireInfos() {
        clearScreen();
        System.out.println("Voici les questions, veuillez répondre à ceux-ci dans l'ordre présenté.");
        System.out.println("1. Numéro de compte du visiteur: ");
        System.out.println("2. Numéro d'assurance maladie du visiteur (SANS ESPACE) sous le format [ABCD12345678]: ");
        System.out.println("3. La date de la visite sous le format [YYYY-MM-DD]: ");
        System.out.println("4. Avez-vous déjà reçu une première dose? [Oui, Non]: ");
        System.out.println("5. Avez-vous déjà contracté la COVID? [Oui, Non]: ");
        System.out.println("6. Avez-vous des symptômes de la COVID? [Oui, Non]: ");
        System.out.println("7. Avez-vous des allergies? [Oui, Non]: ");
        System.out.println("8. Quel vaccin souhaitez-vous recevoir? [Moderna, Pfizer, AstraZeneca, Janssen]: ");
    }

    /**
     * Méthode pour affichage demande information pour questionnaire cherché.
     */
    public void requestGetQuestionnaireInfos() {
        clearScreen();
        System.out.println("1. Numéro de compte du visiteur: ");
    }

    /**
     * Méthode pour affichage questionnaire à imprimer.
     * @param visiteur Visiteur cible
     * @param numeroAssuranceMaladie numéro assurance maladie
     * @param dateVisite date visite pour vaccin
     * @param reponsePremiereDose réponse pour si première dose ou non
     * @param reponseContracteCovid réponse si déjà eu covid
     * @param reponseSymptomeCovid réponse pour si a symptômes
     * @param reponseAllergie réponse pour si a allergie
     * @param preferenceVaccin réponse pour préférence vaccin
     */
    public void requestPrintQuestionnaire(Visiteur visiteur, String numeroAssuranceMaladie, String dateVisite, String reponsePremiereDose, String reponseContracteCovid, String reponseSymptomeCovid, String reponseAllergie, String preferenceVaccin) {
        clearScreen();
        System.out.println("INFORMATIONS REMPLIES PAR L'EMPLOYÉ");
        System.out.println("__________________________________________________________");
        System.out.println("Numéro de compte du visiteur: " + visiteur.getNumeroCompte());
        System.out.println("Nom du visiteur: " + visiteur.getNom());
        System.out.println("Prénom du visiteur: " + visiteur.getPrenom());
        System.out.println("Date de naissance du visiteur [YYYY-MM-DD]: " + new SimpleDateFormat("yyyy-MM-dd").format(visiteur.getDateNaissance()));
        System.out.println("Numéro d'assurance maladie du visiteur [SANS ESPACE]: " + numeroAssuranceMaladie);
        System.out.println("La date de la visite sous le format [YYYY-MM-DD]: " + dateVisite);
        System.out.println("Avez-vous déjà reçu une première dose? [Oui, Non]: " + reponsePremiereDose);
        System.out.println("Avez-vous déjà contracté la COVID? [Oui, Non]: " + reponseContracteCovid);
        System.out.println("Avez-vous des symptômes de la COVID? [Oui, Non]: " + reponseSymptomeCovid);
        System.out.println("Avez-vous des allergies? [Oui, Non]: " + reponseAllergie);
        System.out.println("Quel vaccin souhaitez-vous recevoir? [Moderna, Pfizer, AstraZeneca, Janssen]: " + preferenceVaccin);
        System.out.println();
        System.out.println("INFORMATIONS REMPLIES PAR LE PROFESSIONNEL DE LA SANTÉ");
        System.out.println("__________________________________________________________");
        System.out.println("Avez-vous procédé à la vaccination? [Oui, Non]: ");
        System.out.println("Nom du vaccin [Moderna, Pfizer, AstraZeneca, Janssen]: ");
        System.out.println("Code du vaccin (24 caractères max): ");
        System.out.println("Lot du vaccin (6 caractères): ");
        System.out.println();
        System.out.println("Voulez-vous imprimer ce questionnaire rempli?");
        System.out.println("1. Oui");
        System.out.println("2. Non, retour à la page menu.");
        maxOption = 2;
    }

    /**
     * Méthode pour imprimer liste questionnaire d'un visiteur.
     * @param liste liste de questionnaire
     */
    public void requestPrintQuestionnaire(LinkedList<Questionnaire> liste) {
        clearScreen();
        for (Questionnaire questionnaire : liste) {
            System.out.println("INFORMATIONS REMPLIES PAR L'EMPLOYÉ");
            System.out.println("__________________________________________________________");
            System.out.println("Numéro de compte du visiteur: " + questionnaire.getNumeroCompte());
            System.out.println("Nom du visiteur: " + questionnaire.getNom());
            System.out.println("Prénom du visiteur: " + questionnaire.getPrenom());
            System.out.println("Date de naissance du visiteur [YYYY-MM-DD]: " + questionnaire.getDateNaissance());
            System.out.println("Numéro d'assurance maladie du visiteur [SANS ESPACE]: " + questionnaire.getNumeroAssuranceMaladie());
            System.out.println("La date de la visite sous le format [YYYY-MM-DD]: " + questionnaire.getDateVisite());
            System.out.println("Avez-vous déjà reçu une première dose? [Oui, Non]: " + questionnaire.getReponsePremiereDose());
            System.out.println("Avez-vous déjà contracté la COVID? [Oui, Non]: " + questionnaire.getReponseContracteCovid());
            System.out.println("Avez-vous des symptômes de la COVID? [Oui, Non]: " + questionnaire.getReponseSymptomeCovid());
            System.out.println("Avez-vous des allergies? [Oui, Non]: " + questionnaire.getReponseAllergie());
            System.out.println("Quel vaccin souhaitez-vous recevoir? [Moderna, Pfizer, AstraZeneca, Janssen]: " + questionnaire.getPreferenceVaccin());
            System.out.println();
            System.out.println("INFORMATIONS REMPLIES PAR LE PROFESSIONNEL DE LA SANTÉ");
            System.out.println("__________________________________________________________");
            System.out.println("Avez-vous procédé à la vaccination? [Oui, Non]: ");
            System.out.println("Nom du vaccin [Moderna, Pfizer, AstraZeneca, Janssen]: ");
            System.out.println("Code du vaccin (24 caractères max): ");
            System.out.println("Lot du vaccin (6 caractères): ");
            System.out.println();
        }
        System.out.println("Voulez-vous imprimer ce questionnaire rempli?");
        System.out.println("1. Oui");
        System.out.println("2. Non, retour à la page menu.");
        maxOption = 2;
    }

    /**
     * Méthode pour afficher page liste de bénévole.
     * @param liste liste bénévole
     */
    public void afficherListeBenevole(ArrayList<Benevole> liste) {
        clearScreen();
        for (Benevole benevole : liste) {
            System.out.printf("%1$-1s %2$-10s %3$-1s %4$-10s", "Nom: ", benevole.getNom(), "Prénom: ", benevole.getPrenom());
            System.out.println();
            System.out.println("Modification bénévole succès. Peser sur [ENTER] pour retourner à la page menu.");
        }
    }

    /**
     * Méthode pour afficher page liste rendez-vous dans prochains 48 heures.
     * @param liste liste rendez-vous dans prochains 48 heures
     */
    public void afficherProchainRendezVous(ArrayList<RendezVous> liste) {
        clearScreen();
        for (RendezVous rdv : liste) {
            System.out.printf("%1$-1s %2$-10s %3$-1s %4$-10s %5$-1s %6$-15s %7$-1s %8$-20s %9$-1s %10$-15d", "Numéro de réservation: ", rdv.getNumeroReservation(), "Heure de visite: ", rdv.getHeureVisite()+":00", "Nom: ", rdv.getNom(), "Prénom: ", rdv.getPrenom(), "Type dose: ", rdv.getTypeDose());
            System.out.println();
        }
    }

    /**
     * Méthode pour afficher profil de vaccination.
     * @param profilVaccination profil de vaccination à montrer
     * @param visiteur Visiteur cible
     */
    public void afficherProfilVaccination(ProfilVaccination profilVaccination, Visiteur visiteur) {
        clearScreen();
        System.out.println("Rapport de vaccination produit");
        System.out.println("Nom: " + visiteur.getPrenom() + " " + visiteur.getNom());
        System.out.println("Dose: " + profilVaccination.getTypeDose());
        System.out.println("Vaccin reçu: " + profilVaccination.getNomVaccin());
        System.out.println("Date vaccination: " + profilVaccination.getDateVaccination());
        System.out.println("Code QR: " + profilVaccination.getCodeQr());
    }

    /**
     * Méthode pour savoir si authentifié ou non.
     * @return true si authentifié, sinon non
     */
    public Boolean isAuthenticated() {
        return utilisateurConnecte.getCodeIdentification() != null && utilisateurConnecte.getMotDePasse() != null;
    }

    /**
     * Méthode pour clear la console.
     */
    public void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    /**
     * Méthode pour afficher message.
     * @param message message à afficher
     */
    public void promptMessage(String message) {
        System.out.println(message);
    }

    /**
     * Méthode pour avoir nombre maximum d'options.
     * @return nombre d'option possible
     */
    public int getMaxOption() {
        return maxOption;
    }

    /**
     * Méthode pour modifier maxOption.
     * @param i nouveau maxOption
     */
    public void setMaxOption(int i) {
        this.maxOption = i;
    }
}
