import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Représente un compte utilisateur.
 */
public class CompteUtilisateur extends Compte {
    /*
        ID_GENERATOR: Générateur de ID automatique
        codeIdentification: Code d'identification de l'employé
        motDePasse: Mot de passe de l'employé
        codePostal: Code postal de l'employé
        ville: Ville où se situe l'employé
        type: Type d'utilisateur
        typeValues: Types d"utilisateurs possibles
     */
    private static final AtomicInteger ID_GENERATOR = new AtomicInteger(100000000);
    private String codeIdentification;
    private String motDePasse;
    private String adresse;
    private String codePostal;
    private String ville;
    private final String type;
    private enum typeValues {
        BENEVOLE,
        EMPLOYE
    }

    /**
     * Constructeur sans param.
     */
    public CompteUtilisateur() {
        super(null, null, null, null, null, null);
        this.codeIdentification = null;
        this.motDePasse = null;
        this.adresse = null;
        this.codePostal = null;
        this.ville = null;
        this.type = null;
    }

    /**
     * Constructeur avec param.
     * @param numeroCompte numéro compte utilisateur
     * @param nom nom utilisateur
     * @param prenom prénom utilisateur
     * @param dateNaissance date de naissance utilisateur
     * @param adresseCourriel adresse courriel utilisateur
     * @param numeroTelephone numéro de téléphone utilisateur
     * @param motDePasse mot de passe utilisateur
     * @param adresse adresse utilisateur
     * @param codePostal code postal utilisateur
     * @param ville ville utilisateur
     * @param type type utilisateur
     * @throws ParseException SimpleDateFormat peut causer un parse exception
     */
    public CompteUtilisateur(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone, String motDePasse, String adresse, String codePostal, String ville, int type) throws ParseException {
        super(numeroCompte, nom, prenom, new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance), adresseCourriel, numeroTelephone);
        if (numeroCompte != null && nom != null && prenom != null && !dateNaissance.equals("0000-00-00") && adresseCourriel != null && numeroTelephone != null && motDePasse != null && adresse != null && codePostal != null && ville != null) {
            this.codeIdentification = Integer.toString(ID_GENERATOR.getAndIncrement());
            this.motDePasse = motDePasse;
            this.adresse = adresse;
            this.codePostal = codePostal;
            this.ville = ville;
        }
        if (type == 0) {
            this.type = typeValues.BENEVOLE.toString();
        } else {
            this.type = typeValues.EMPLOYE.toString();
        }
    }

    /**
     * Méthode pour avoir code identification.
     * @return code identification utilisateur
     */
    public String getCodeIdentification() {
        return codeIdentification;
    }

    /**
     * Méthode pour modifier code identification.
     * @param codeIdentification nouveau code identification
     */
    public void setCodeIdentification(String codeIdentification) {
        this.codeIdentification = codeIdentification;
    }

    /**
     * Méthode pour avoir mot de passe.
     * @return mot de passe utilisateur
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * Méthode pour modifier mot de passe.
     * @param motDePasse nouveau mot de passe
     */
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    /**
     * Méthode pour avoir adresse.
     * @return adresse utilisateur
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Méthode pour modifier adresse.
     * @param adresse nouvelle adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Méthode pour avoir code postal.
     * @return code postal utilisateur
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * Méthode pour modifier code postal.
     * @param codePostal nouveau code postal
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Méthode pour avoir ville.
     * @return ville utilisateur
     */
    public String getVille() {
        return ville;
    }

    /**
     * Méthode pour modifier ville.
     * @param ville nouvelle ville
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Méthode pour avoir type utilisateur.
     * @return type utilisateur
     */
    public String getType() {
        return type;
    }
}
