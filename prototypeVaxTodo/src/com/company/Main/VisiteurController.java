import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controlleur qui gère les requêtes reliées au visiteur.
 */
public class VisiteurController {

    Database database;
    Visiteur model;
    Menu view;

    /**
     * Constructeur VisiteurController.
     * @param database database où les données se trouvent
     * @param model données d'un visiteur
     * @param view affichage interface pour utilisateur
     */
    public VisiteurController(Database database, Visiteur model, Menu view) {
        this.database = database;
        this.model = model;
        this.view = view;
    }

    /**
     * Méthode pour valider inputs pour ajout visiteur.
     * @param numeroCompte numéro compte visiteur
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateNaissance date de naissance du visiteur
     * @param adresseCourriel email visiteur
     * @param numeroTelephone numéro téléphone visiteur
     * @return true si inputs valides, sinon false
     */
    public Boolean validationAjoutVisiteur(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone) {
        boolean valid = true;
        if(!database.getNumeroCompte(numeroCompte)) {
            view.promptMessage("Le numéro de compte est déjà utilisé par quelqu'un d'autre.");
            valid = false;
        }
        if (numeroCompte.length() != 12) {
            view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            valid = false;
        }
        if(nom.length() > 50) {
            view.promptMessage("Le nom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        if(prenom.length() > 50) {
            view.promptMessage("Le prénom ne peut pas dépasser 50 caractères.");
            valid = false;
        }

        try {
            new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
        } catch (ParseException e) {
            view.promptMessage("La date de naissance n'a pas été donnée dans le bon format.");
            valid = false;
        }

        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        Matcher matcher = pattern.matcher(adresseCourriel);
        if (!matcher.matches()) {
            view.promptMessage("L'adresse courriel n'a pas été donnée dans le bon format.");
            valid = false;
        }

        pattern = Pattern.compile("^(\\d{3}[- .]?){2}\\d{4}$");
        matcher = pattern.matcher(numeroTelephone);
        if (!matcher.matches()) {
            view.promptMessage("Le numéro de téléphone n'a pas été donné dans le bon format.");
            valid = false;
        }
        return valid;
    }

    /**
     * Méthode pour ajouter visiteur.
     * @param visiteur nouveau visiteur
     * @return true si ajouté avec succès, sinon false
     */
    public Boolean ajoutVisiteur(Visiteur visiteur) {
        model = visiteur;
        return database.addVisiteur(model);
    }

    /**
     * Méthode pour valider inputs supprimer visiteur.
     * @param numeroCompte numéro comptre visiteur
     * @return true si inputs valides, sinon false
     */
    public Boolean validationSupprimerVisiteur(String numeroCompte) {
        boolean valid = true;
        model = database.getVisiteur(numeroCompte);
        if (numeroCompte.length() != 12){
            view.promptMessage("Numero de compte n'est pas de 12 chiffres.");
            valid = false;
        }
        if (numeroCompte.length() == 12) {
            if (model == null) {
                view.promptMessage("Le visiteur cherché n'existe pas.");
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Méthode pour supprimer visiteur.
     * @param numeroCompte numéro compte visiteur
     * @return true si supprimé avec succès, sinon false
     */
    public Boolean supprimerVisiteur(String numeroCompte) {
        return database.supprimerVisiteur(numeroCompte);
    }

    /**
     * Méthode validation inputs modifier visiteur.
     * @param numeroCompte numéro compte visiteur
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateNaissance date de naissance du visiteur
     * @param adresseCourriel email visiteur
     * @param numeroTelephone numéro téléphone visiteur
     * @return true si inputs valides, sinon false
     */
    public Boolean validationModifierVisiteur(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone) {
        boolean valid = true;
        if(database.getVisiteur(numeroCompte) == null) {
            view.promptMessage("Le numéro de compte n'est pas associé à un compte.");
            valid = false;
            if (numeroCompte.length() != 12) {
                view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            }
        } else {
            if (!nom.isEmpty() && !nom.isBlank()) {
                if (nom.length() > 50) {
                    view.promptMessage("Le nom ne peut pas dépasser 50 caractères.");
                    valid = false;
                }
            }

            if (!prenom.isEmpty() && !prenom.isBlank()) {
                if (prenom.length() > 50) {
                    view.promptMessage("Le prénom ne peut pas dépasser 50 caractères.");
                    valid = false;
                }
            }

            if (!dateNaissance.isEmpty() && !dateNaissance.isBlank()) {
                try {
                    new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
                } catch (ParseException e) {
                    view.promptMessage("La date de naissance n'a pas été donnée dans le bon format.");
                    valid = false;
                }
            }

            Pattern pattern = Pattern.compile("^(.+)@(.+)$");
            Matcher matcher = pattern.matcher(adresseCourriel);
            if (!adresseCourriel.isEmpty() && !adresseCourriel.isBlank()) {
                if (!matcher.matches()) {
                    view.promptMessage("L'adresse courriel n'a pas été donnée dans le bon format.");
                    valid = false;
                }
            }

            pattern = Pattern.compile("^(\\d{3}[- .]?){2}\\d{4}$");
            matcher = pattern.matcher(numeroTelephone);
            if (!numeroTelephone.isEmpty() && !numeroTelephone.isBlank()) {
                if (!matcher.matches()) {
                    view.promptMessage("Le numéro de téléphone n'a pas été donné dans le bon format.");
                    valid = false;
                }
            }
        }
        return valid;
    }

    /**
     * Méthode pour modifier visiteur.
     * @param numeroCompte numéro compte visiteur
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateNaissance date de naissance du visiteur
     * @param adresseCourriel email visiteur
     * @param numeroTelephone numéro téléphone visiteur
     * @return true si visiteur modifié avec succès, sinon false
     * @throws ParseException SimpleDateFormat peut avoir erreur de parse
     */
    public Boolean modifierVisiteur(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone) throws ParseException {
        Visiteur cible = database.getVisiteur(numeroCompte);
        if (!nom.isEmpty() && !nom.isBlank()) {
            cible.setNom(nom);
        }
        if (!prenom.isEmpty() && !prenom.isBlank()) {
            cible.setPrenom(prenom);
        }
        if (!dateNaissance.isEmpty() && !prenom.isBlank()) {
            cible.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance));
        }
        if (!adresseCourriel.isEmpty() && !adresseCourriel.isBlank()) {
            cible.setAdresseCourriel(adresseCourriel);
        }
        if (!numeroTelephone.isEmpty() && !numeroTelephone.isBlank()) {
            cible.setNumeroTelephone(numeroTelephone);
        }
        return database.updateVisiteur(cible);
    }

    /**
     * Méthode validation inputs profil vaccination.
     * @param numeroCompte numéro compte visiteur
     * @param dateVaccination date vaccination
     * @param typeDose type dose
     * @param nomVaccin nom du vaccin
     * @param codeVaccin code du vaccin
     * @param lotVaccin lot du vaccin
     * @return true si inputs valides, sinon false
     */
    public Boolean validationProfilVaccination(String numeroCompte, String dateVaccination, int typeDose, String nomVaccin, String codeVaccin, String lotVaccin) {
        boolean valid = true;
        enum vaccins {
            Moderna("Moderna"),
            Pfizer("Pfizer"),
            AstraZeneca("AstraZeneca"),
            Janssen("Janssen");

            private String vaccinChoisi;

            vaccins(String vaccinChoisi) {
                this.vaccinChoisi = vaccinChoisi;
            }

            public String getVaccinChoisi() {
                return vaccinChoisi;
            }
        }

        if (numeroCompte.length() != 12) {
            view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            if (database.getVisiteur(numeroCompte) == null) {
                view.promptMessage("Numéro de compte invalide.");
            }
            valid = false;
        }

        String patternDate = "yyyy-MM-dd";
        Date now = Calendar.getInstance().getTime();
        try {
            Date date = new SimpleDateFormat(patternDate).parse(dateVaccination);
            String dateToString = new SimpleDateFormat(patternDate).format(now);
            if (date.after(now)) {
                if (!dateToString.equals(dateVaccination)) {
                    view.promptMessage("La date de vaccination n'est pas valide.");
                    valid = false;
                }
            }
        } catch (ParseException e) {
            view.promptMessage("La date de vaccination n'a pas été donnée dans le bon format.");
            valid = false;
        }

        if (typeDose < 1 || typeDose > 2) {
            view.promptMessage("Le type de dose est invalide.");
            valid = false;
        }

        boolean contains = false;
        for (vaccins v : vaccins.values()) {
            if (v.name().equals(nomVaccin)) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            view.promptMessage("Le nom du vaccin rentré n'est pas parmi les choix possibles.");
            valid = false;
        }

        if (codeVaccin.length() > 24) {
            view.promptMessage("Le code du vaccin rentré excède 24 caractères");
            valid = false;
        }

        if (lotVaccin.length() > 6) {
            view.promptMessage("Le lot du vaccin rentré excède 6 caractères");
            valid = false;
        }

        return valid;
    }

    /**
     * Méthode pour ajouter profil vaccination.
     * @param numeroCompte numéro compte visiteur
     * @param dateVaccination date vaccination
     * @param typeDose type dose
     * @param nomVaccin nom du vaccin
     * @param codeVaccin code du vaccin
     * @param lotVaccin lot du vaccin
     * @return true si ajouté profil vaccination avec succès, sinon false
     */
    public Boolean ajoutProfilVaccination(String numeroCompte, String dateVaccination, int typeDose, String nomVaccin, String codeVaccin, String lotVaccin) {
        ProfilVaccination profilVaccination = new ProfilVaccination(numeroCompte, dateVaccination, typeDose, nomVaccin, codeVaccin, lotVaccin, generateCodeQr());
        Visiteur visiteur = database.getVisiteur(numeroCompte);
        boolean requestAjoutProfilSuccess = visiteur.ajoutProfilVaccination(profilVaccination);
        boolean requestUpdateVisiteurSuccess = false;
        if (requestAjoutProfilSuccess) {
            requestUpdateVisiteurSuccess = database.updateVisiteur(visiteur);
        }
        if (requestUpdateVisiteurSuccess) {
            sendEmail();
            view.afficherProfilVaccination(profilVaccination, visiteur);
        }
        return requestUpdateVisiteurSuccess;
    }

    /**
     * Méthode pour générer code QR.
     * @return code QR
     */
    public String generateCodeQr() {
        return "####################";
    }

    /**
     * Méthode pour envoyer courriel.
     */
    public void sendEmail() {
        view.promptMessage("COURRIEL ENVOYÉ AVEC SUCCÈS À LA PERSONNE CONCERNÉE");
    }

    /**
     * Méthode pour avoir visiteur.
     * @param numeroCompte numéro compte visiteur
     * @return visiteur cherché
     */
    public Visiteur getVisiteur(String numeroCompte) {
        model = database.getVisiteur(numeroCompte);
        return model;
    }
}
