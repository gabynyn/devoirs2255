import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * VaxTodo est un logiciel utilisé pour aider les employés de la compagnie GoodPeople pour la prise de rendez-vous de vaccin.
 * De plus, cet outil les aide pour faire des suivis pour les visiteurs qui se présentent au centre de vaccination.
 * @author Gaby Nguyen, Wen Yin, Taha Zakariya
 * @version 1.0
 */
public class Main {

    /**
     * La méthode main est où toutes les opérations se passent. Ceci qui inclut: connection au système, consultation de la liste de rendez-vous,
     * consultation du calendrier, gestion des visiteurs, gestion des bénévoles, remplissage du questionnaire pour la vaccination, consultation de la liste des
     * prochaine(s) visite(s), la déconnexion au système et la fermeture de l'application.
     * @param args pas utilisé dans ce logiciel
     * @throws Exception Cette exception est présente dans le cas où il y a une erreur de parse.
     */
    public static void main(String[] args) throws Exception {
        /*
         Habituellement, il y aurait une vraie base de donnée donc cette variable n'existerait pas.
         */
        Database database = new Database();

        /*
         Utilisé comme model pour les controlleurs
         */
        CompteUtilisateur user = new CompteUtilisateur();
        RendezVous rendezVous = new RendezVous();
        Visiteur visiteur = new Visiteur();
        Benevole benevole = new Benevole();
        Questionnaire questionnaire = new Questionnaire();

        /*
         Utilisé pour afficher interface dépendamment de la requête de l'utilisateur
         */
        Menu view = new Menu(user);
        /*
         Utilisé pour la gestion des requêtes reliées au login
         */
        LoginController loginController = new LoginController(database, user, view);
        /*
         Utilisé pour la gestion des requêtes reliées aux rendez-vous
         */
        RendezVousController rendezVousController = new RendezVousController(database, rendezVous, view);
        /*
         Utilisé pour la gestion des requêtes reliées au visiteur
         */
        VisiteurController visiteurController = new VisiteurController(database, visiteur, view);
        /*
         Utilisé pour la gestion des requêtes reliées au bénévole
         */
        BenevoleController benevoleController = new BenevoleController(database, benevole, view);
        /*
         Utilisé pour la gestion des requêtes reliées au questionnaire
         */
        QuestionnaireController questionnaireController = new QuestionnaireController(database, questionnaire, view);

        /*
         Variables utilisés pour avoir l'input de l'utilisateur et pour la gestion des inputs de celui-ci
         */
        Scanner scanner = new Scanner(System.in);
        int selectedOption = 0;
        String userInput;
        String command = "START";

        /*
         Remplissage database pour tester et début app
         */
        remplirDatabase(database);

        /*
         Fonctionnement application
         */
        do {
            switch(command) {
                /*
                 Page accueil de l'application
                 */
                case "START" -> {
                    do {
                        view.start();
                        userInput = scanner.nextLine();
                        if (isNumeric(userInput)) {
                            selectedOption = Integer.parseInt(userInput);
                        }
                        switch (selectedOption) {
                            case 1 -> {
                                do {
                                    user = loginController.login();
                                } while (!view.isAuthenticated());
                                command = "MENU";
                            }
                            case 2 -> {
                                view.exit();
                                command = "EXIT";
                            }
                        }
                    } while (!validationOption(view, userInput));
                }
                /*
                 Page menu adaptée aux autorisations de l'utilisateur
                 */
                case "MENU" -> {
                    do {
                        switch (user.getType()) {
                            case "BENEVOLE" -> view.afficherPageBenevole();
                            case "EMPLOYE" -> view.afficherPageEmploye();
                        }
                        userInput = scanner.nextLine();
                        if (isNumeric(userInput)) {
                            selectedOption = Integer.parseInt(userInput);
                        }
                    } while (!validationOption(view, userInput));
                    if(user.getType().equals("BENEVOLE")) {
                        command = selectBenevole(selectedOption);
                    } else {
                        command = selectEmploye(selectedOption);
                    }
                }
                /*
                 Page affichant liste de rendez-vous de la journée
                 */
                case "LISTERDV" -> {
                    ArrayList<RendezVous> listeRdv = rendezVousController.getListeRendezVous();
                    if (listeRdv.size() > 0) {
                        System.in.read();
                    }
                    command = "MENU";
                }
                /*
                 Page affichant calendrier
                 */
                case "CALENDRIER" -> {

                    boolean valid;
                    int nbPersonne;
                    do {
                        view.requestCalendrierNbPersonne();
                        userInput = scanner.nextLine();

                        if (!isNumeric(userInput)) {
                            view.promptMessage("Il faut entrer l'un des chiffres proposés.");
                        }
                    } while(!isNumeric(userInput));

                    do {
                        view.requestCalendrierOption();
                        userInput = scanner.nextLine();
                        valid = validationOption(view, userInput);
                    } while(!valid);

                    nbPersonne = Integer.parseInt(userInput);
                    if(nbPersonne == 1) {
                        rendezVousController.getCalendrier(nbPersonne);
                    }
                    if (nbPersonne == 2) {
                        do {
                            view.requestCalendrierDate();
                            userInput = scanner.nextLine();

                            String patternDate = "yyyy-MM-dd";
                            try {
                                new SimpleDateFormat(patternDate).parse(userInput);
                                valid = true;
                            } catch (ParseException e) {
                                view.promptMessage("La date de visite n'a pas été donnée dans le bon format.");
                                valid = false;
                            }
                        } while (!valid);
                        rendezVousController.getCalendrier(nbPersonne, userInput);
                    }

                    do {
                        view.requestAjoutRendezVous();
                        userInput = scanner.nextLine();
                        if (isNumeric(userInput)) {
                            selectedOption = Integer.parseInt(userInput);
                        }
                    } while (!validationOption(view, userInput));
                    command = selectCalendrier(selectedOption);
                }
                /*
                 Page pour requête ajout rendez-vous
                 */
                case "AJOUTRDV" -> {
                    String nom;
                    String prenom;
                    String dateVisite;
                    String heureVisite;
                    int typeDose;
                    int nbPersonne;
                    boolean valid;
                    do {
                        view.requestRendezVousInfos();
                        System.out.print("1. ");
                        nom = scanner.nextLine();
                        System.out.print("2. ");
                        prenom = scanner.nextLine();
                        System.out.print("3. ");
                        dateVisite = scanner.nextLine();
                        System.out.print("4. ");
                        heureVisite = scanner.nextLine();
                        System.out.print("5. ");
                        userInput = scanner.nextLine();
                        typeDose = 0;
                        if(isNumeric(userInput)) {
                            typeDose = Integer.parseInt(userInput);
                        }
                        System.out.print("6. ");
                        userInput = scanner.nextLine();
                        nbPersonne = 0;
                        if(isNumeric(userInput)) {
                            nbPersonne = Integer.parseInt(userInput);
                        }
                        valid = rendezVousController.validationRendezVous(nom, prenom, dateVisite, heureVisite, typeDose, nbPersonne);
                        if (valid) {
                            valid = rendezVousController.validationDispo(dateVisite, heureVisite, nbPersonne);
                        }
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("AJOUTRDV"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = rendezVousController.ajouterRendezVous(new RendezVous(nom, prenom, dateVisite, heureVisite, typeDose, nbPersonne));
                        if (requestSuccess) {
                            view.promptMessage("Ajout rendez-vous succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Ajout rendez-vous échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour gestion des visiteurs
                */
                case "GVISITEUR" -> {
                    do {
                        view.requestVisiteurOption();
                        userInput = scanner.nextLine();
                    } while (!validationOption(view, userInput));
                    selectedOption = Integer.parseInt(userInput);
                    command = selectGestionVisiteur(selectedOption);
                }
                /*
                 Page pour l'ajout d'un visiteur
                 */
                case "AVISITEUR" -> {
                    String numeroCompte;
                    String nom;
                    String prenom;
                    String dateNaissance;
                    String adresseCourriel;
                    String numeroTelephone;
                    boolean valid;
                    do {
                        view.requestVisiteurInfos();
                        System.out.print("1. ");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2. ");
                        nom = scanner.nextLine();
                        System.out.print("3. ");
                        prenom = scanner.nextLine();
                        System.out.print("4. ");
                        dateNaissance = scanner.nextLine();
                        System.out.print("5. ");
                        adresseCourriel = scanner.nextLine();
                        System.out.print("6. ");
                        numeroTelephone = scanner.nextLine();
                        valid = visiteurController.validationAjoutVisiteur(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone);
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("AVISITEUR"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = visiteurController.ajoutVisiteur(new Visiteur(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone));
                        if (requestSuccess) {
                            view.promptMessage("Ajout visiteur succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Ajout visiteur échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour supprimer d'un visiteur
                 */
                case "SVISITEUR" -> {
                    boolean valid;
                    do {
                        view.requestSupprimerVisiteurInfos();
                        userInput = scanner.nextLine();
                        valid = visiteurController.validationSupprimerVisiteur(userInput);
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while(!valid && command.equals("SVISITEUR"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = visiteurController.supprimerVisiteur(userInput);
                        if (requestSuccess) {
                            view.promptMessage("Supprimation visiteur succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Supprimation visiteur échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour modifier d'un visiteur
                 */
                case "MVISITEUR" -> {
                    String numeroCompte;
                    String nom;
                    String prenom;
                    String dateNaissance;
                    String adresseCourriel;
                    String numeroTelephone;
                    boolean valid;
                    do {
                        view.promptMessage("Veuillez laisser vide les informations dont vous ne modifié pas sauf pour le numéro du compte.");
                        view.requestVisiteurInfos();
                        System.out.print("1. ");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2. ");
                        nom = scanner.nextLine();
                        System.out.print("3. ");
                        prenom = scanner.nextLine();
                        System.out.print("4. ");
                        dateNaissance = scanner.nextLine();
                        System.out.print("5. ");
                        adresseCourriel = scanner.nextLine();
                        System.out.print("6. ");
                        numeroTelephone = scanner.nextLine();
                        valid = visiteurController.validationModifierVisiteur(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone);
                        if (!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("MVISITEUR"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = visiteurController.modifierVisiteur(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone);
                        if (requestSuccess) {
                            view.promptMessage("Modification visiteur succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Modification visiteur échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour l'ajout d'un profil de vaccination d'un visiteur
                 */
                case "PVISITEUR" -> {
                    String numeroCompte;
                    String dateVaccination;
                    int typeDose = 0;
                    String nomVaccin;
                    String codeVaccin;
                    String lotVaccin;
                    boolean valid;
                    do {
                        view.requestProfilVaccinationInfos();
                        System.out.print("1.");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2.");
                        dateVaccination = scanner.nextLine();
                        System.out.print("3.");
                        userInput = scanner.nextLine();
                        if (isNumeric(userInput)) {
                            typeDose = Integer.parseInt(userInput);
                        }
                        System.out.print("4.");
                        nomVaccin = scanner.nextLine();
                        System.out.print("5.");
                        codeVaccin = scanner.nextLine();
                        System.out.print("6.");
                        lotVaccin = scanner.nextLine();
                        valid = visiteurController.validationProfilVaccination(numeroCompte, dateVaccination, typeDose, nomVaccin, codeVaccin, lotVaccin);
                        if (!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("PVISITEUR"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = visiteurController.ajoutProfilVaccination(numeroCompte, dateVaccination, typeDose, nomVaccin, codeVaccin, lotVaccin);
                        if (requestSuccess) {
                            view.promptMessage("Ajout profil vaccination succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Ajout profil vaccination échoué. Possibilité que type dose invalide. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour la gestion de bénévole
                 */
                case "GBENEVOLE" -> {
                    do {
                        view.requestBenevoleOption();
                        userInput = scanner.nextLine();
                    } while (!validationOption(view, userInput));
                    selectedOption = Integer.parseInt(userInput);
                    command = selectGestionBenevole(selectedOption);
                }
                /*
                 Page pour l'ajout d'un bénévole
                 */
                case "ABENEVOLE" -> {
                    String numeroCompte;
                    String motDePasse;
                    String nom;
                    String prenom;
                    String dateNaissance;
                    String adresse;
                    String codePostal;
                    String ville;
                    String adresseCourriel;
                    String numeroTelephone;
                    boolean valid;
                    do {
                        view.requestAjoutBenevoleInfos();
                        System.out.print("1. ");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2. ");
                        motDePasse = scanner.nextLine();
                        System.out.print("3. ");
                        nom = scanner.nextLine();
                        System.out.print("4. ");
                        prenom = scanner.nextLine();
                        System.out.print("5. ");
                        dateNaissance = scanner.nextLine();
                        System.out.print("6. ");
                        adresse = scanner.nextLine();
                        System.out.print("7. ");
                        codePostal = scanner.nextLine();
                        System.out.print("8. ");
                        ville = scanner.nextLine();
                        System.out.print("9. ");
                        adresseCourriel = scanner.nextLine();
                        System.out.print("10. ");
                        numeroTelephone = scanner.nextLine();
                        valid = benevoleController.validationAjoutBenevole(numeroCompte, motDePasse, nom, prenom, dateNaissance, adresse, codePostal, ville, adresseCourriel, numeroTelephone);
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("ABENEVOLE"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = benevoleController.ajoutBenevole(new Benevole(numeroCompte, nom, prenom, dateNaissance, adresseCourriel, numeroTelephone, motDePasse, adresse, codePostal, ville));
                        if (requestSuccess) {
                            view.promptMessage("Ajout bénévole succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Ajout bénévole échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour supprimer un bénévole
                 */
                case "SBENEVOLE" -> {
                    boolean valid;
                    do {
                        view.requestSupprimerBenevoleInfos();
                        userInput = scanner.nextLine();
                        valid = benevoleController.validationSupprimerBenevole(userInput);
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while(!valid && command.equals("SBENEVOLE"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = benevoleController.supprimerBenevole(userInput);
                        if (requestSuccess) {
                            view.promptMessage("Supprimation bénévole succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Supprimation bénévole échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour modifier un bénévole
                 */
                case "MBENEVOLE" -> {
                    String numeroCompte;
                    String motDePasse;
                    String nom;
                    String prenom;
                    String dateNaissance;
                    String adresse;
                    String codePostal;
                    String ville;
                    String adresseCourriel;
                    String numeroTelephone;
                    boolean valid;
                    do {
                        view.requestModifierBenevoleInfos();
                        System.out.print("1. ");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2. ");
                        motDePasse = scanner.nextLine();
                        System.out.print("3. ");
                        nom = scanner.nextLine();
                        System.out.print("4. ");
                        prenom = scanner.nextLine();
                        System.out.print("5. ");
                        dateNaissance = scanner.nextLine();
                        System.out.print("6. ");
                        adresse = scanner.nextLine();
                        System.out.print("7. ");
                        codePostal = scanner.nextLine();
                        System.out.print("8. ");
                        ville = scanner.nextLine();
                        System.out.print("9. ");
                        adresseCourriel = scanner.nextLine();
                        System.out.print("10. ");
                        numeroTelephone = scanner.nextLine();
                        valid = benevoleController.validationModifierBenevole(numeroCompte, motDePasse, nom, prenom, dateNaissance, adresse, codePostal, ville, adresseCourriel, numeroTelephone);
                        if(!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("MBENEVOLE"));
                    if (!command.equals("MENU")) {
                        boolean requestSuccess = benevoleController.modifierBenevole(numeroCompte, motDePasse, nom, prenom, dateNaissance, adresse, codePostal, ville, adresseCourriel, numeroTelephone);
                        if (requestSuccess) {
                            view.promptMessage("Modification bénévole succès. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        } else {
                            view.promptMessage("Modification bénévole échoué. Peser sur [ENTER] pour retourner à la page menu.");
                            System.in.read();
                        }
                        command = "MENU";
                    }
                }
                /*
                 Page pour liste bénévole(s)
                 */
                case "LBENEVOLE" -> {
                    benevoleController.getListeBenevole();
                    System.in.read();
                    command = "MENU";
                }
                /*
                 Page pour remplir questionnaire
                 */
                case "QUESTIONNAIRE" -> {
                    String numeroCompte;
                    String numeroAssuranceMaladie;
                    String dateVisite;
                    String reponsePremiereDose;
                    String reponseContracteCovid;
                    String reponseSymptomeCovid;
                    String reponseAllergie;
                    String preferenceVaccin;
                    boolean valid;
                    do {
                        view.requestQuestionnaireInfos();
                        System.out.print("1.");
                        numeroCompte = scanner.nextLine();
                        System.out.print("2.");
                        numeroAssuranceMaladie = scanner.nextLine();
                        System.out.print("3.");
                        dateVisite = scanner.nextLine();
                        System.out.print("4.");
                        reponsePremiereDose = scanner.nextLine();
                        System.out.print("5.");
                        reponseContracteCovid = scanner.nextLine();
                        System.out.print("6.");
                        reponseSymptomeCovid = scanner.nextLine();
                        System.out.print("7.");
                        reponseAllergie = scanner.nextLine();
                        System.out.print("8.");
                        preferenceVaccin = scanner.nextLine();
                        valid = questionnaireController.validationQuestionnaire(numeroCompte, numeroAssuranceMaladie, dateVisite, reponsePremiereDose, reponseContracteCovid, reponseSymptomeCovid, reponseAllergie, preferenceVaccin);
                        if (!valid) {
                            command = redo(view, command);
                        }
                    } while(!valid && command.equals("QUESTIONNAIRE"));
                    if (!command.equals("MENU")) {
                        do {
                            view.requestPrintQuestionnaire(visiteurController.getVisiteur(numeroCompte), numeroAssuranceMaladie, dateVisite, reponsePremiereDose, reponseContracteCovid, reponseSymptomeCovid, reponseAllergie, preferenceVaccin);
                            userInput = scanner.nextLine();
                        } while (!validationOption(view, userInput));
                        selectedOption = Integer.parseInt(userInput);
                        switch (selectedOption) {
                            case 1 -> command = "PQUESTIONNAIRE";
                            case 2 -> command = "MENU";
                        }
                    }
                }
                /*
                 Avoir questionnaire d'un visiteur
                 */
                case "GQUESTIONNAIRE" -> {
                    boolean valid;
                    LinkedList<Questionnaire> liste;
                    do {
                        view.requestGetQuestionnaireInfos();
                        userInput = scanner.nextLine();
                        valid = questionnaireController.validationGetQuestionnaire(userInput);
                        if (!valid) {
                            command = redo(view, command);
                        }
                    } while (!valid && command.equals("GQUESTIONNAIRE"));
                    if (!command.equals("MENU")) {
                        do {
                            liste = questionnaireController.getQuestionnaire(userInput);
                            view.requestPrintQuestionnaire(liste);
                            userInput = scanner.nextLine();
                        } while (!validationOption(view, userInput));
                        selectedOption = Integer.parseInt(userInput);
                        switch (selectedOption) {
                            case 1 -> command = "PQUESTIONNAIRE";
                            case 2 -> command = "MENU";
                        }
                    }
                }
                /*
                 Imprimer questionnaire
                 */
                case "PQUESTIONNAIRE" -> {
                    questionnaireController.printQuestionnaire();
                    System.in.read();
                    command = "MENU";
                }
                /*
                 Page pour avoir les visites dans prochain 48 heures
                 */
                case "PROCHAINVISITE" -> {
                    rendezVousController.getProchaineVisite();
                    System.in.read();
                    command = "MENU";
                }
                /*
                 Déconnexion du système
                 */
                case "DECONNEXION" -> {
                    loginController.logout(user);
                    command = "START";
                }
                case "EXIT" -> System.exit(0);
            }
        } while (command != null);
    }

    /**
     * Méthode pour valider si l'option choisi par l'utilisateur est valide.
     * @param view pour notifier l'utilisateur s'il y a une erreur dans l'option qu'il a choisi
     * @param selectedOption input de l'utilisateur
     * @return true si valide, sinon false
     */
    public static Boolean validationOption(Menu view, String selectedOption) {
        if (!isNumeric(selectedOption)) {
            view.promptMessage("Charactère entré n'est pas un chiffre.");
            return false;
        } else if (Integer.parseInt(selectedOption) > view.getMaxOption() || Integer.parseInt(selectedOption) < 0) {
            view.promptMessage("Option choisie invalide.");
            return false;
        }
        return true;
    }

    /**
     * Méthode utilisé pour voir si le string est un nombre ou non.
     * @param str input à vérifier
     * @return true si est un nombre, sinon false
     */
    public static Boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    /**
     * Méthode utilisé pour option choisi à la page calendrier.
     * @param selectedOption option choisi de l'utilisateur
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String selectCalendrier(int selectedOption) {
        switch(selectedOption) {
            case 1 -> {
                return "AJOUTRDV";
            }
            case 2 -> {
                return "MENU";
            }
        }
        return null;
    }

    /**
     * Méthode utilisé pour option choisi à la page menu du bénévole.
     * @param selectedOption option choisi de l'utilisateur
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String selectBenevole(int selectedOption){
        switch(selectedOption) {
            case 1 -> {
                return "LISTERDV";
            }
            case 2 -> {
                return "CALENDRIER";
            }
            case 3 -> {
                return "DECONNEXION";
            }
        }
        return null;
    }

    /**
     * Méthode utilisé pour option choisi à la page menu de l'employé.
     * @param selectedOption option choisi de l'utilisateur
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String selectEmploye(int selectedOption){
        switch(selectedOption) {
            case 1 -> {
                return "LISTERDV";
            }
            case 2 -> {
                return "CALENDRIER";
            }
            case 3 -> {
                return "GVISITEUR";
            }
            case 4 -> {
                return "GBENEVOLE";
            }
            case 5 -> {
                return "QUESTIONNAIRE";
            }
            case 6 -> {
                return "PROCHAINVISITE";
            }
            case 7 -> {
                return "GQUESTIONNAIRE";
            }
            case 8 -> {
                return "DECONNEXION";
            }
        }
        return null;
    }

    /**
     * Méthode utilisé pour option choisi à la page gestion visiteur.
     * @param selectedOption option choisi de l'utilisateur
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String selectGestionVisiteur(int selectedOption){
        switch(selectedOption) {
            case 1 -> {
                return "AVISITEUR";
            }
            case 2 -> {
                return "SVISITEUR";
            }
            case 3 -> {
                return "MVISITEUR";
            }
            case 4 -> {
                return "PVISITEUR";
            }
            case 5 -> {
                return "MENU";
            }
        }
        return null;
    }

    /**
     * Méthode utilisé pour option choisi à la page gestion bénévole.
     * @param selectedOption option choisi de l'utilisateur
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String selectGestionBenevole(int selectedOption){
        switch(selectedOption) {
            case 1 -> {
                return "ABENEVOLE";
            }
            case 2 -> {
                return "SBENEVOLE";
            }
            case 3 -> {
                return "MBENEVOLE";
            }
            case 4 -> {
                return "LBENEVOLE";
            }
            case 5 -> {
                return "MENU";
            }
        }
        return null;
    }

    /**
     * Méthode utilisé pour offrir l'option de recommencer ce que l'utilisateur faisait ou de retourner à la page menu.
     * @param view pour afficher les options possibles
     * @param command action dont l'utilisateur était en train de faire
     * @return commande en fonction de l'option choisi de l'utilisateur
     */
    public static String redo(Menu view, String command){
        String userInput;
        Scanner scanner = new Scanner(System.in);
        int selectedOption;
        do {
            view.promptMessage("Informations rentrées ne sont pas valides.");
            view.promptMessage("1. Recommencer");
            view.promptMessage("2. Retour au menu");
            view.setMaxOption(2);
            userInput = scanner.nextLine();
        } while (!validationOption(view, userInput));
        selectedOption = Integer.parseInt(userInput);
        switch (selectedOption) {
            case 1 -> {
                return command;
            }
            case 2 -> {
                return "MENU";
            }
        }
        return command;
    }

    /**
     * Méthode pour remplir la base de donnée fictive.
     * @param database simulation base de donnée
     * @throws Exception au cas qu'il y a une exception de parse
     */
    public static void remplirDatabase(Database database) throws Exception {
        Employe employeTest = new Employe("123456789123", "Nguyen", "Gaby", "2000-07-19", "gaby.nguyen@umontreal.ca", "5140000000", "test", "99 avenue Test", "h2k3f9", "Montréal");
        Benevole benevoleTest = new Benevole("123456789000", "tested", "test", "1981-09-20", "test@umontreal.ca", "5141230000", "testb", "100 avenue Test", "w2l3f1", "Montréal");
        Visiteur visiteurTest = new Visiteur("000000000000", "Tester", "Test", "1970-10-10", "email@umontreal.ca", "5143450000");
        Questionnaire questionnaireTest = new Questionnaire("000000000000", "Tester", "Test", "1970-10-10", "TEST12345678", "2021-12-15", "NON", "NON", "NON", "NON", "MODERNA","OUI", "PFIZER", "52", "123456");
        database.addQuestionnaire(questionnaireTest);
        database.addUser(employeTest);
        database.addUser(benevoleTest);
        database.addVisiteur(visiteurTest);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date now = Calendar.getInstance().getTime();
        String date = dateFormat.format(now);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date dateProchain1 = cal.getTime();
        String dateNext1 = dateFormat.format(dateProchain1);
        cal.add(Calendar.DATE, 1);
        Date dateProchain2 = cal.getTime();
        String dateNext2 = dateFormat.format(dateProchain2);

        RendezVous rdv1 = new RendezVous("Nguyen", "Gaby", date, "18", 1, 2);
        RendezVous rdv2 = new RendezVous("Yin", "Wen", dateNext1, "08", 2, 1);
        RendezVous rdv3 = new RendezVous("Zakariya", "Taha", dateNext2, "10", 1, 1);
        RendezVous rdv4 = new RendezVous("Ducoin", "Aurélien", "2021-11-08", "12", 2, 1);
        RendezVous rdv5 = new RendezVous("Lafontant", "Louis-Édouard", "2021-11-08", "18", 1, 2);
        database.addRendezVous(rdv1);
        database.addRendezVous(rdv2);
        database.addRendezVous(rdv3);
        database.addRendezVous(rdv4);
        database.addRendezVous(rdv5);
    }
}
