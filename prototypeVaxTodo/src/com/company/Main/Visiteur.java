import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Représente visiteur
 */
public class Visiteur extends Compte {

    private HashMap<Integer, ProfilVaccination> vaccinRecu = new HashMap<>();

    /**
     * Constructeur Visiteur sans param.
     */
    public Visiteur() {
        super(null, null, null, null, null, null);
    }


    /**
     * Constructeur Visiteur avec param.
     * @param numeroCompte numéro compte visiteur
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateNaissance date de naissance du visiteur
     * @param adresseCourriel email visiteur
     * @param numeroTelephone numéro téléphone visiteur
     * @throws ParseException SimpleDateFormat.parse peut avoir un parse exception
     */
    public Visiteur(String numeroCompte, String nom, String prenom, String dateNaissance, String adresseCourriel, String numeroTelephone) throws ParseException {
        super(numeroCompte, nom, prenom, new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance), adresseCourriel, numeroTelephone);
    }

    /**
     * Méthode ajout profil vaccination
     * @param profilVaccination nouveau profil vaccination
     * @return true si profil vaccination valide, sinon false
     */
    public Boolean ajoutProfilVaccination(ProfilVaccination profilVaccination) {
        if(vaccinRecu.get(profilVaccination.getTypeDose()) != null) {
            return false;
        } else {
            vaccinRecu.put(profilVaccination.getTypeDose(), profilVaccination);
        }
        return true;
    }
}
