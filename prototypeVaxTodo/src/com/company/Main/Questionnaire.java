/**
 * Représente questionnaire
 */
public class Questionnaire {
    private String numeroCompte;
    private String nom;
    private String prenom;
    private String dateNaissance;
    private String numeroAssuranceMaladie;
    private String dateVisite;
    private String reponsePremiereDose;
    private String reponseContracteCovid;
    private String reponseSymptomeCovid;
    private String reponseAllergie;
    private String preferenceVaccin;
    private String reponseProcedeVaccin;
    private String nomVaccin;
    private String codeVaccin;
    private String lotVaccin;

    /**
     * Constructeur Questionnaire sans param.
     */
    public Questionnaire() {
        this.numeroCompte = null;
        this.nom = null;
        this.prenom = null;
        this.dateNaissance = null;
        this.numeroAssuranceMaladie = null;
        this.dateVisite = null;
        this.reponsePremiereDose = null;
        this.reponseContracteCovid = null;
        this.reponseSymptomeCovid = null;
        this.reponseAllergie = null;
        this.preferenceVaccin = null;
        this.reponseProcedeVaccin = null;
        this.nomVaccin = null;
        this.codeVaccin = null;
        this.lotVaccin = null;
    }

    /**
     * Constructeur Questionnaire avec param
     * @param numeroCompte numéro compte visiteur
     * @param nom nom visiteur
     * @param prenom prénom visiteur
     * @param dateNaissance date naissance visiteur
     * @param numeroAssuranceMaladie numéro assurance maladie visiteur
     * @param dateVisite date visite
     * @param reponsePremiereDose réponse première dose
     * @param reponseContracteCovid réponse contracté covid
     * @param reponseSymptomeCovid réponse symptôme covid
     * @param reponseAllergie réponse allergie
     * @param preferenceVaccin réponse préférence vaccin
     * @param reponseProcedeVaccin réponse procédé vaccin
     * @param nomVaccin nom vaccin
     * @param codeVaccin code vaccin
     * @param lotVaccin lot vaccin
     */
    public Questionnaire(String numeroCompte, String nom, String prenom, String dateNaissance, String numeroAssuranceMaladie, String dateVisite, String reponsePremiereDose, String reponseContracteCovid, String reponseSymptomeCovid, String reponseAllergie, String preferenceVaccin, String reponseProcedeVaccin, String nomVaccin, String codeVaccin, String lotVaccin) {
        this.numeroCompte = numeroCompte;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.numeroAssuranceMaladie = numeroAssuranceMaladie;
        this.dateVisite = dateVisite;
        this.reponsePremiereDose = reponsePremiereDose;
        this.reponseContracteCovid = reponseContracteCovid;
        this.reponseSymptomeCovid = reponseSymptomeCovid;
        this.reponseAllergie = reponseAllergie;
        this.preferenceVaccin = preferenceVaccin;
        this.reponseProcedeVaccin = reponseProcedeVaccin;
        this.nomVaccin = nomVaccin;
        this.codeVaccin = codeVaccin;
        this.lotVaccin = lotVaccin;
    }

    /**
     * Méthode pour avoir numeroCompte.
     * @return numéro du compte
     */
    public String getNumeroCompte() {
        return numeroCompte;
    }

    /**
     * Méthode pour modifier numeroCompte.
     * @param numeroCompte nouveau numéro de compte
     */
    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    /**
     * Méthode pour avoir nom.
     * @return nom de la personne
     */
    public String getNom() {
        return nom;
    }

    /**
     * Méthode pour modifier nom.
     * @param nom nouveau nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode pour avoir prénom.
     * @return prénom de la personne
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Méthode pour modifier prenom.
     * @param prenom nouveau prénom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode pour avoir la date de naissance.
     * @return date de naissance de la personne
     */
    public String getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Méthode pour modifier la date de naissance.
     * @param dateNaissance nouvelle date de naissance
     */
    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Méthode pour avoir numéro assurance maladie.
     * @return numéro assurance maladie
     */
    public String getNumeroAssuranceMaladie() {
        return numeroAssuranceMaladie;
    }

    /**
     * Méthode pour modifier numéro assurance maladie.
     * @param numeroAssuranceMaladie nouveau numéro assurance maladie
     */
    public void setNumeroAssuranceMaladie(String numeroAssuranceMaladie) {
        this.numeroAssuranceMaladie = numeroAssuranceMaladie;
    }

    /**
     * Méthode pour avoir date de visite.
     * @return date de visite
     */
    public String getDateVisite() {
        return dateVisite;
    }

    /**
     * Méthode pour modifier date de vistite
     * @param dateVisite nouvelle date de visite
     */
    public void setDateVisite(String dateVisite) {
        this.dateVisite = dateVisite;
    }

    /**
     * Méthode pour avoir réponse question première dose.
     * @return réponse question première dose.
     */
    public String getReponsePremiereDose() {
        return reponsePremiereDose;
    }

    /**
     * Méthode pour modifer réponse question première dose.
     * @param reponsePremiereDose nouvelle réponse question première dose.
     */
    public void setReponsePremiereDose(String reponsePremiereDose) {
        this.reponsePremiereDose = reponsePremiereDose;
    }

    /**
     * Méthode pour avoir réponse question contracté la covid.
     * @return réponse question contracté la covid
     */
    public String getReponseContracteCovid() {
        return reponseContracteCovid;
    }

    /**
     * Méthode pour modifier réponse contracté la covid.
     * @param reponseContracteCovid nouvelle réponse contracté la covid
     */
    public void setReponseContracteCovid(String reponseContracteCovid) {
        this.reponseContracteCovid = reponseContracteCovid;
    }

    /**
     * Méthode pour avoir réponse symptôme covid.
     * @return réponse symptôme covid
     */
    public String getReponseSymptomeCovid() {
        return reponseSymptomeCovid;
    }

    /**
     * Méthode pour modifier réponse symptôme covid.
     * @param reponseSymptomeCovid nouvelle réponse symptôme covid
     */
    public void setReponseSymptomeCovid(String reponseSymptomeCovid) {
        this.reponseSymptomeCovid = reponseSymptomeCovid;
    }

    /**
     * Méthode pour avoir réponse allergie.
     * @return réponse allergie
     */
    public String getReponseAllergie() {
        return reponseAllergie;
    }

    /**
     * Méthode pour modifier réponse allergie.
     * @param reponseAllergie nouvelle réponse allergie
     */
    public void setReponseAllergie(String reponseAllergie) {
        this.reponseAllergie = reponseAllergie;
    }

    /**
     * Méthode pour avoir préférence vaccin
     * @return préférence vaccin
     */
    public String getPreferenceVaccin() {
        return preferenceVaccin;
    }

    /**
     * Méthode pour modifier préférence vaccin
     * @param preferenceVaccin nouvelle préférence vaccin
     */
    public void setPreferenceVaccin(String preferenceVaccin) {
        this.preferenceVaccin = preferenceVaccin;
    }

    /**
     * Méthode pour avoir réponse procédé vaccin
     * @return réponse procédé vaccin
     */
    public String getReponseProcedeVaccin() {
        return reponseProcedeVaccin;
    }

    /**
     * Méthode pour modifier procédé vaccin
     * @param reponseProcedeVaccin nouvelle réponse procédé vaccin
     */
    public void setReponseProcedeVaccin(String reponseProcedeVaccin) {
        this.reponseProcedeVaccin = reponseProcedeVaccin;
    }

    /**
     * Méthode pour avoir nom vaccin.
     * @return nom vaccin
     */
    public String getNomVaccin() {
        return nomVaccin;
    }

    /**
     * Méthode pour modifier nom vaccin.
     * @param nomVaccin nouveau nom vaccin
     */
    public void setNomVaccin(String nomVaccin) {
        this.nomVaccin = nomVaccin;
    }

    /**
     * Méthode pour avoir code vaccin.
     * @return code vaccin
     */
    public String getCodeVaccin() {
        return codeVaccin;
    }

    /**
     * Méthode pour modifier code vaccin.
     * @param codeVaccin nouveau code vaccin
     */
    public void setCodeVaccin(String codeVaccin) {
        this.codeVaccin = codeVaccin;
    }

    /**
     * Méthode pour avoir lot vaccin.
     * @return lot vaccin
     */
    public String getLotVaccin() {
        return lotVaccin;
    }

    /**
     * Méthode pour modifier lot vaccin.
     * @param lotVaccin nouveau lot vaccin
     */
    public void setLotVaccin(String lotVaccin) {
        this.lotVaccin = lotVaccin;
    }
}
