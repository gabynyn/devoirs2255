/**
 * Controlleur qui gère les requêtes reliées au login.
 */
public class LoginController {

    private Database database;
    private CompteUtilisateur model;
    private Menu view;

    /**
     * Constructeur du controlleur.
     * @param database database où les données se trouvent
     * @param model données d'un utilisateur
     * @param view affichage interface pour utilisateur
     */
    public LoginController(Database database, CompteUtilisateur model, Menu view) {
        this.database = database;
        this.model = model;
        this.view = view;
    }

    /**
     * Méthode pour se connecter au système.
     * @return utilisateur si authentifié, sinon null
     */
    public CompteUtilisateur login() {
        view.afficherLogin();
        Boolean verificationResponse = verificationLogin(model.getCodeIdentification(), model.getMotDePasse());
        if (verificationResponse) {
                model = database.getUser(model.getCodeIdentification());
                view.isAuthenticated();
                return model;
        }
        System.out.println("Le code d'identification ou le mot de passe sont mauvais. Svp réessayer.");
        model.setCodeIdentification(null);
        model.setMotDePasse(null);
        return null;
    }

    /**
     * Méthode pour se déconnecter du système.
     * @param utilisateur utilisateur connecté
     * @return vrai si bon utilisateur, sinon false
     */
    public Boolean logout(CompteUtilisateur utilisateur) {
        if(utilisateur == model) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param codeIdentification code identification utilisateur
     * @param motDePasse mot de passe utilisateur
     * @return true si bonnes informations, sinon false
     */
    public Boolean verificationLogin(String codeIdentification, String motDePasse) {
        CompteUtilisateur cible = database.getUser(codeIdentification);
        if (cible == null) {
            return false;
        } else {
            return cible.getMotDePasse().equals(motDePasse);
        }
    }
}
