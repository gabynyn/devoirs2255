import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controlleur qui gère les requêtes reliées au questionnaire.
 */
public class QuestionnaireController {
    private Database database;
    private Questionnaire model;
    private Menu view;

    /**
     * Constructeur QuestionnaireController
     * @param database database où les données se trouvent
     * @param model données d'un questionnaire
     * @param view affichage interface pour utilisateur
     */
    public QuestionnaireController(Database database, Questionnaire model, Menu view) {
        this.database = database;
        this.model = model;
        this.view = view;
    }

    /**
     * Méthode pour valider inputs pour questionnaire
     * @param numeroCompte numéro compte visiteur
     * @param numeroAssuranceMaladie numéro assurance maladie visiteur
     * @param dateVisite date visite visiteur
     * @param reponsePremiereDose réponse à question première dose
     * @param reponseContracteCovid réponse à contracté covid
     * @param reponseSymptomeCovid réponse à symptôme covid
     * @param reponseAllergie réponse à allergie
     * @param preferenceVaccin préférence vaccin
     * @return true si valide inputs, sinon false
     */
    public Boolean validationQuestionnaire(String numeroCompte, String numeroAssuranceMaladie, String dateVisite, String reponsePremiereDose, String reponseContracteCovid, String reponseSymptomeCovid, String reponseAllergie, String preferenceVaccin) {
        boolean valid = true;

        enum vaccins {
            Moderna("Moderna"),
            Pfizer("Pfizer"),
            AstraZeneca("AstraZeneca"),
            Janssen("Janssen");

            private String vaccinChoisi;

            vaccins(String vaccinChoisi) {
                this.vaccinChoisi = vaccinChoisi;
            }

            public String getVaccinChoisi() {
                return vaccinChoisi;
            }
        }

        if (numeroCompte.length() != 12) {
            view.promptMessage("Le numéro de compte n'est pas de la bonne longueur.");
            valid = false;
        }

        if (database.getVisiteur(numeroCompte) == null) {
            view.promptMessage("Numéro de compte invalide.");
            valid = false;
        }

        Pattern pattern = Pattern.compile("[A-Za-z]{4}[0-9]{8}");
        Matcher matcher = pattern.matcher(numeroAssuranceMaladie);
        if (!matcher.matches()) {
            view.promptMessage("Le numéro d'assurance maladie ne correspond pas au format suggeré.");
            valid = false;
        }

        String patternDate = "yyyy-MM-dd";
        Date now = Calendar.getInstance().getTime();
        try {
            Date date = new SimpleDateFormat(patternDate).parse(dateVisite);
            String dateToString = new SimpleDateFormat(patternDate).format(now);
            if (date.before(now)) {
                if (!dateToString.equals(dateVisite)) {
                    view.promptMessage("La date de visite n'est pas valide.");
                    valid = false;
                }
            }
        } catch (ParseException e) {
            view.promptMessage("La date de visite n'a pas été donnée dans le bon format.");
            valid = false;
        }

        if (!reponsePremiereDose.equalsIgnoreCase("OUI")) {
            if (!reponsePremiereDose.equalsIgnoreCase("NON")) {
                view.promptMessage("La réponse pour la question 4 ne peut qu'être oui ou non.");
                valid = false;
            }
        }

        if (!reponseContracteCovid.equalsIgnoreCase("OUI")) {
            if (!reponseContracteCovid.equalsIgnoreCase("NON")) {
                view.promptMessage("La réponse pour la question 5 ne peut qu'être oui ou non.");
                valid = false;
            }
        }

        if (!reponseSymptomeCovid.equalsIgnoreCase("OUI")) {
            if (!reponseSymptomeCovid.equalsIgnoreCase("NON")) {
                view.promptMessage("La réponse pour la question 6 ne peut qu'être oui ou non.");
                valid = false;
            }
        }
        if (!reponseAllergie.equalsIgnoreCase("OUI")) {
            if (!reponseAllergie.equalsIgnoreCase("NON")) {
                view.promptMessage("La réponse pour la question 7 ne peut qu'être oui ou non.");
                valid = false;
            }
        }

        boolean contains = false;
        for (vaccins v : vaccins.values()) {
            if (v.name().equalsIgnoreCase(preferenceVaccin)) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            view.promptMessage("La réponse à la question 8 ne correspond pas aux vaccins proposés.");
            valid = false;
        }

        return valid;
    }

    /**
     * Méthode pour valider si input pour get questionnaire valid.
     * @param numeroCompte numéro compte visiteur
     * @return true si input valide, sinon false
     */
    public Boolean validationGetQuestionnaire(String numeroCompte) {
        return database.getVisiteur(numeroCompte) != null;
    }

    /**
     * Méthode pour avoir questionnaire.
     * @param numeroCompte numéro compte visiteur
     * @return liste avec questionnaire
     */
    public LinkedList<Questionnaire> getQuestionnaire(String numeroCompte) {
        return database.getQuestionnaire(numeroCompte);
    }

    /**
     * Méthode pour imprimer questionnaire.
     */
    public void printQuestionnaire() {
        view.promptMessage("Questionnaire imprimé avec succès. Peser sur [ENTER] pour retourner à la page menu.");
    }
}
