
import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginControllerTest {

    Database database;
    Menu menu;
    static CompteUtilisateur user;
    LoginController loginController;

    @BeforeAll
    protected static void setupUser() throws ParseException {
        user  = new CompteUtilisateur("000000000000",  "Tester", "Test","1970-10-10", "email@umontreal.ca", "5143450000","test19701010", "Downtown","H1Z2T3", "Montreal", 0);
    }

    @BeforeEach
    protected void setup() throws ParseException {
        database = new Database();
        menu = new Menu(user);
        database.addUser(user);
        loginController = new LoginController(database, user, menu);
    }

    @DisplayName("le login fonctionne (mdp et identification correct), Fonctionne")
    @Test
    public void loginValideTest() {
        assertTrue(loginController.verificationLogin("100000000","test19701010"));
    }

    @DisplayName("indentification entree incorrect")
    @Test
    public void loginMauvaisIdentificationEntreeTest() {
        assertFalse(loginController.verificationLogin("111111111111","test19701010"));
    }

    @DisplayName("mot de passe entree incorrect")
    @Test
    public void loginMauvaisMdpEntreeTest() {
        assertFalse(loginController.verificationLogin("100000000","test12345678"));
    }

    @DisplayName("logout user fonctionne")
    @Test
    public void logoutTest() {
        assertTrue(loginController.logout(user));
    }

    @DisplayName("mauvais utilisateur entree")
    @Test
    public void logoutInvalideTest() {
        assertFalse(loginController.logout(null));
    }

}

