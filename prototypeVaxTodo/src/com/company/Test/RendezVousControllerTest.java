import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class RendezVousControllerTest {

    RendezVousController rendezVousController;
    Database database;
    Menu menu;
    RendezVous rendezVous;
    CompteUtilisateur user;
    RendezVous rdv1;
    RendezVous rdv2;
    RendezVous rdv3;

    @BeforeEach
    protected void setup() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date now = Calendar.getInstance().getTime();
        String date = dateFormat.format(now);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date dateProchain1 = cal.getTime();
        String dateNext1 = dateFormat.format(dateProchain1);
        cal.add(Calendar.DATE, 1);
        Date dateProchain2 = cal.getTime();
        String dateNext2 = dateFormat.format(dateProchain2);

        rdv1 = new RendezVous("Nguyen", "Gaby", date, "18", 1, 2);
        rdv2 = new RendezVous("Yin", "Wen", dateNext1, "08", 2, 1);
        rdv3 = new RendezVous("Zakariya", "Taha", dateNext2, "10", 1, 1);
        Visiteur visiteurTest = new Visiteur("000000000000", "Tester", "Test", "1970-10-10", "email@umontreal.ca", "5143450000");

        user = new CompteUtilisateur();
        database = new Database();
        menu = new Menu(user);
        rendezVous = new RendezVous();

        database.addVisiteur(visiteurTest);
        database.addRendezVous(rdv1);
        database.addRendezVous(rdv2);
        database.addRendezVous(rdv3);
        rendezVousController = new RendezVousController(database, rendezVous, menu);
    }

    @DisplayName("Get liste rendez-vous, fonctionne")
    @Test
    public void getListeRendezVousTest() {
        ArrayList<RendezVous> listeComparaison = new ArrayList<>();
        listeComparaison.add(rdv1);
        assertEquals(listeComparaison, rendezVousController.getListeRendezVous());
    }

    @DisplayName("Validation nom, fonctionne")
    @Test
    public void validationNomTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Validation nom plus de 50 caractères")
    @Test
    public void validationNom50CharTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("123456789012345678901234567890123456789012345678901", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Validation prénom, fonctionne")
    @Test
    public void validationPrenomTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Validation prénom plus de 50 caractères")
    @Test
    public void validationPrenom50CharTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "123456789012345678901234567890123456789012345678901", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Date visite vérification fonctionne")
    @Test
    public void dateVisiteTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Date visite mauvaise entrée")
    @Test
    public void dateVisiteMauvaiseEntreeTest() {
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", "@", "12", 1, 2));
    }

    @DisplayName("Date visite avant")
    @Test
    public void dateVisiteAvantTest() {
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", "2021-12-17", "12", 1, 2));
    }

    @DisplayName("Date visite après")
    @Test
    public void dateVisiteApresTest() {
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", "2024-12-17", "12", 1, 2));
    }

    @DisplayName("Heure visite vérification fonctionne")
    @Test
    public void heureVisiteTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Heure visite mauvaise entrée")
    @Test
    public void heureVisiteMauvaiseEntreeTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "a", 1, 2));
    }

    @DisplayName("Heure visite avant")
    @Test
    public void heureVisiteAvantTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "2", 1, 2));
    }

    @DisplayName("Heure visite après")
    @Test
    public void heureVisiteApresTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "19", 1, 2));
    }

    @DisplayName("Type dose = 1, fonctionne")
    @Test
    public void typeDose1Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Type dose = 2, fonctionne")
    @Test
    public void typeDose2Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 2, 2));
    }

    @DisplayName("Type dose = autre entrée")
    @Test
    public void typeDoseAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 3, 2));
    }

    @DisplayName("Nombre personne = 1, fonctionne")
    @Test
    public void nbPersonne1Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 1));
    }

    @DisplayName("Nombre personne = 2, fonctionne")
    @Test
    public void nbPersonne2Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 2));
    }

    @DisplayName("Nombre personne = autre entrée")
    @Test
    public void nbPersonneAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationRendezVous("Tested", "Test", new SimpleDateFormat(patternDate).format(now), "12", 1, 3));
    }

    @DisplayName("Validation disponibilité fonctionne")
    @Test
    public void validationDispoTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(rendezVousController.validationDispo(new SimpleDateFormat(patternDate).format(now), "12", 1));
    }

    @DisplayName("Validation disponibilité nb personne au-dessus de 40")
    @Test
    public void validationDispoDepasse30Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(rendezVousController.validationDispo(new SimpleDateFormat(patternDate).format(now), "12", 31));
    }

    @DisplayName("Ajout rendez-vous fonctionne")
    @Test
    public void ajoutRdvTest() {
        RendezVous rdvTest = new RendezVous("Ducoin", "Aurélien", "2021-11-08", "12", 2, 1);
        assertTrue(rendezVousController.ajouterRendezVous(rdvTest));
    }

    @DisplayName("Ajout rendez-vous = null")
    @Test
    public void ajoutRdvNullTest() {
        assertFalse(rendezVousController.ajouterRendezVous(null));
    }
}