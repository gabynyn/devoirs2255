
import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MainTest {
    CompteUtilisateur user;
    Menu view;
    Database database;
    Main main;

    @BeforeEach
    protected void setup() throws ParseException {
        user = new CompteUtilisateur();
        database = new Database();
        view = new Menu(user);
        main = new Main();
    }

    @DisplayName("le selected option entree est valide, fonctionne")
    @Test
    public void validationOptionReussiTest() {
        String selectedOption = "0";
        assertTrue(main.validationOption(view, selectedOption));
    }

    @DisplayName("le selected option entree est invalide")
    @Test
    public void validationOptionInvalideTest() {
        String selectedOption = "-1";
        assertFalse(main.validationOption(view, selectedOption));
    }

    @DisplayName("la valeur entree est une valeur numeric, Fonctionne")
    @Test
    public void isNumericValideTest() {
        String str = "1";
        assertTrue(main.isNumeric(str));
    }

    @DisplayName("la valeur entree nest pas une valeur numeric")
    @Test
    public void isNumericInvalideTest() {
        String str = "b";
        assertFalse(main.isNumeric(str));
    }

    @DisplayName("loption choisie par le user est valide, fonctionne")
    @Test
    public void selectCalendrierValideTest() {
        int selectedOption = 1;
        String expectedResult = "AJOUTRDV";
        assertTrue(main.selectCalendrier(selectedOption) == expectedResult);
    }

    @DisplayName("loption choisie par le user est valide (nexiste pas)")
    @Test
    public void selectCalendrierInvalideTest() {
        int selectedOption = 5;
        assertTrue(main.selectCalendrier(selectedOption) == null);
    }

    @DisplayName("loption choisie par le user est valide et return le bon la bonne commande en fct de loption choisie, fonctionne")
    @Test
    public void selectBenevoleValideTest() {
        int selectedOption = 2;
        String expectedResult = "CALENDRIER";
        assertTrue(main.selectBenevole(selectedOption) == expectedResult);
    }

    @DisplayName("loption choisie par le user nest pas valide (nexiste pas)")
    @Test
    public void selectBenevoleInvalideTest() {
        int selectedOption = 10;
        assertTrue(main.selectBenevole(selectedOption) == null);
    }

    @DisplayName("loption choisie par le user est valide et return le bon la bonne commande en fct de loption choisie, fonctionne")
    @Test
    public void selectGestionBenevoleValideTest() {
        int selectedOption = 3;
        String expectedResult = "MBENEVOLE";
        assertTrue(main.selectGestionBenevole(selectedOption) == expectedResult);
    }

    @DisplayName("loption choisie par le user nest pas valide (nexiste pas/nest pas une option possible)")
    @Test
    public void selectGestionBenevoleInvalideTest() {
        int selectedOption = 7;
        assertTrue(main.selectGestionBenevole(selectedOption) == null);
    }


}