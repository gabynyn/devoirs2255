import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BenevoleControllerTest {
    BenevoleController benevoleController;
    Database database;
    Menu menu;
    Benevole benevole;
    Employe employe;


    @BeforeEach
    protected void setup() throws ParseException {
        database = new Database();
        benevole = new Benevole("000000000000","Tester","Test","1970-10-10",
                "email@umontreal.ca","5143450000","test19701010","Downtown",
                "H1Z2T3","Montreal");
        benevole.setMotDePasse("test19701011@");
        employe = new Employe("000000000000","Tester","Test","1970-10-10",
                "email@umontreal.ca","5143450000","test19701010","Downtown",
                "H1Z2T3","Montreal");
        menu = new Menu(benevole);
        database.addBenevole(benevole);

        benevoleController = new BenevoleController(database, benevole, menu);
    }

    @DisplayName("ajout d'un bénévole vérification fonctionne")
    @Test
    public void ajoutBenevoleTest() {
        assertTrue(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","5143450000"));

    }

    @DisplayName("Le numéro de compte est déjà utilisé par quelqu'un d'autre.")
    @Test
    public void numCompteExiste() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000000","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le numéro de compte n'est pas de la bonne longueur.")
    @Test
    public void numCompteMauvaisFormat() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001", "test19701011",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 caractère spécial.")
    @Test
    public void motPassIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("0000000000001", "test19701011@"
                ,"Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le nom ne peut pas dépasser 50 caractères.")
    @Test
    public void nomMauvaisFormat() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001", "test19701011@",
                "Tester1Tester1Tester1Tester1Tester1Tester1Tester1Tester1", "Test1", "1970-10-11"
                , "Downtown", "H1Z2T3", "Montreal", "email@umontreal.ca",
                "5143450000"));
    }

    @DisplayName("Le prénom ne peut pas dépasser 50 caractères.")
    @Test
    public void prenomMauvaisFormat() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1",
                "1970-10-11","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("La date de naissance n'a pas été donnée dans le bon format.")
    @Test
    public void DateNaissanceMauvaisFormat() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","19701011","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","5143450000"));
    }

    @DisplayName("L'adresse ne peut pas dépasser 100 caractères.")
    @Test
    public void adresseIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11",
                "DowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntown",
                "H1Z2T3","Montreal","email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le code postal ne peut pas dépasser 6 caractères.")
    @Test
    public void codePostalIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3E","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("La ville ne peut pas dépasser 50 caractères.")
    @Test
    public void villeIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "MontrealMontrealMontrealMontrealMontrealMontrealMon","email@umontreal.ca",
                "5143450000"));
    }

    @DisplayName("L'adresse courriel n'a pas été donnée dans le bon format.")
    @Test
    public void emailIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","emailumontrealca","5143450000"));
    }

    @DisplayName("Le numéro de téléphone n'a pas été donné dans le bon format.")
    @Test
    public void numPhoneIncorrect() {
        assertFalse(benevoleController.validationAjoutBenevole("000000000001","test19701011@",
                "Tester1","Test1","1970-10-11","Downtown","H1Z2T3",
                "Montreal","email@umontreal.ca","50000"));
    }


    @DisplayName("supprime d'un bénévole vérification fonctionne")
    @Test
    public void supprimeBenevoleTest() {
        assertTrue(benevoleController.validationSupprimerBenevole(benevole.getCodeIdentification()));
    }

    @DisplayName("Code d'identification n'est pas de 9 chiffres.")
    @Test
    public void codeIdMauvaisFormat() {
        assertFalse(benevoleController.validationSupprimerBenevole("000"));
    }

    @DisplayName("Le bénévole cherché n'existe pas.")
    @Test
    public void benevoleNonExiste() {
        assertFalse(benevoleController.validationSupprimerBenevole("000000002"));
    }

    @DisplayName("Le code d'identification n'est pas associé à un bénévole.")
    @Test
    public void idNonAssocie() {
        assertFalse(benevoleController.validationSupprimerBenevole(employe.getCodeIdentification()));
    }



    @DisplayName("modifie d'un bénévole vérification fonctionne")
    @Test
    public void modifieBenevoleTest() {
        assertTrue(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));

    }

    @DisplayName("Le numéro de compte n'est pas associé à un compte.")
    @Test
    public void numCompteNonAssocie() {
        assertFalse(benevoleController.validationModifierBenevole("111111111",benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le numéro de compte n'est pas de la bonne longueur.")
    @Test
    public void numCompteMauvaisFormatModi() {
        assertFalse(benevoleController.validationModifierBenevole("11111",benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 caractère spécial.")
    @Test
    public void motPassIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),"test19701010",
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le nom ne peut pas dépasser 50 caractères.")
    @Test
    public void nomMauvaisFormatModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "TesterTesterTesterTesterTesterTesterTesterTesterTester","Test","1970-10-10",
                "Downtown","H1Z2T3","Montreal", "email@umontreal.ca",
                "5143450000"));

    }

    @DisplayName("Le prénom ne peut pas dépasser 50 caractères.")
    @Test
    public void prenomMauvaisFormatModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","TestTestTestTestTestTestTestTestTestTestTestTestTest","1970-10-10",
                "Downtown","H1Z2T3","Montreal", "email@umontreal.ca",
                "5143450000"));
    }

    @DisplayName("La date de naissance n'a pas été donnée dans le bon format.")
    @Test
    public void DateNaissanceMauvaisFormatModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","19701010","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("L'adresse ne peut pas dépasser 100 caractères.")
    @Test
    public void adresseIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","DowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntownDowntown",
                "H1Z2T3","Montreal", "email@umontreal.ca","5143450000"));
    }

    @DisplayName("Le code postal ne peut pas dépasser 6 caractères.")
    @Test
    public void codePostalIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z-2T3","Montreal",
                "email@umontreal.ca","5143450000"));
    }

    @DisplayName("La ville ne peut pas dépasser 50 caractères.")
    @Test
    public void villeIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3",
                "MontrealMontrealMontrealMontrealMontrealMontrealMontreal", "email@umontreal.ca",
                "5143450000"));
    }

    @DisplayName("L'adresse courriel n'a pas été donnée dans le bon format.")
    @Test
    public void emailIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "emailumontreal.ca","5143450000"));
    }

    @DisplayName("Le numéro de téléphone n'a pas été donné dans le bon format.")
    @Test
    public void numPhoneIncorrectModi() {
        assertFalse(benevoleController.validationModifierBenevole(benevole.getCodeIdentification(),benevole.getMotDePasse(),
                "Tester","Test","1970-10-10","Downtown","H1Z2T3","Montreal",
                "email@umontreal.ca","00"));
    }


}