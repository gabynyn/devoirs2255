import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VisiteurControllerTest {
    VisiteurController visiteurController;
    Database database;
    Menu menu;
    Visiteur visiteur;
    ProfilVaccination profilVaccination;
    Employe employe;


    @BeforeEach
    protected void setup() throws ParseException {
        database = new Database();
        visiteur = new Visiteur("000000000000","Tester","Test","2002-02-27",
                "email@umontral.ca","4380001111");
        profilVaccination = new ProfilVaccination("000000000000","2021-11-11",
                1,"Moderna","123456","789","qr");
        employe = new Employe("100000000000","Tester","Test","1970-10-10",
                "email@umontreal.ca","5143450000","test19701010","Downtown",
                "H1Z2T3","Montreal");
        visiteur.ajoutProfilVaccination(profilVaccination);
        menu = new Menu(employe);
        database.addVisiteur(visiteur);
        visiteurController = new VisiteurController(database,visiteur,menu);
    }



    @DisplayName("ajout d'un visiteur vérification fonctionne")
    @Test
    public void ajoutVisiteurTest() {
        assertTrue(visiteurController.validationAjoutVisiteur("000000000001","Tester1","Test1",
                "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le numéro de compte est déjà utilisé par quelqu'un d'autre.e")
    @Test
    public void numCompteExiste() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000000","Tester1","Test1",
                "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le numéro de compte n'est pas de la bonne longueur.")
    @Test
    public void numCompteMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("0000000000001","Tester1","Test1",
                "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le nom ne peut pas dépasser 50 caractères.")
    @Test
    public void nomMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000001","Tester1Tester1Tester1Tester1Tester1Tester1Tester1Tester1Tester1",
                "Test1", "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le prénom ne peut pas dépasser 50 caractères.")
    @Test
    public void prenomMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000001","Tester1","Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1",
                "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("La date de naissance n'a pas été donnée dans le bon format.")
    @Test
    public void dateNaissanceMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000001","Tester1","Test1",
                "20020227","email@umontreal.ca","4380001111"));
    }

    @DisplayName("L'adresse courriel n'a pas été donnée dans le bon format.")
    @Test
    public void adresseMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000001","Tester1","Test1",
                "2002-02-27","emailumontreal.ca","4380001111"));
    }

    @DisplayName("Le numéro de téléphone n'a pas été donné dans le bon format.")
    @Test
    public void numPhoneMauvaisFormat() {
        assertFalse(visiteurController.validationAjoutVisiteur("000000000001","Tester1","Test1",
                "2002-02-27","email@umontreal.ca","0001111"));
    }




    @DisplayName("supprime d'un visiteur vérification fonctionne")
    @Test
    public void supprimeVisiteurTest() {
        assertTrue(visiteurController.validationSupprimerVisiteur(visiteur.getNumeroCompte()));

    }

    @DisplayName("Numero de compte n'est pas de 12 chiffres.")
    @Test
    public void numCompteMauvaisformat() {
        assertFalse(visiteurController.validationSupprimerVisiteur("000000000"));
    }

    @DisplayName("Le visiteur cherché n'existe pas.")
    @Test
    public void numCompteNonExiste() {
        assertFalse(visiteurController.validationSupprimerVisiteur("000000000002"));
    }


    @DisplayName("modifie d'un visiteur vérification fonctionne")
    @Test
    public void modifieVisiteurTest() {
        assertTrue(visiteurController.validationModifierVisiteur(visiteur.getNumeroCompte(),visiteur.getNom(),
                visiteur.getPrenom(),visiteur.getDateNaissance().toString(),visiteur.getAdresseCourriel(),
                "5140001111"));
    }

    @DisplayName("Le numéro de compte n'est pas associé à un compte.")
    @Test
    public void compteNumNonAssocie() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000002","Tester1",
                "Test1", "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le numéro de compte n'est pas de la bonne longueur.")
    @Test
    public void compteNumMauvaisFormat() {
        assertFalse(visiteurController.validationModifierVisiteur("0000000000001","Tester1",
                "Test1", "2002-02-27","email@umontreal.ca","4380001111"));
    }

    @DisplayName("Le nom ne peut pas dépasser 50 caractères.")
    @Test
    public void nomMauvaisFormatModi() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000001","Tester1Tester1Tester1Tester1Tester1Tester1Tester1Tester1Tester1",
                "Test1", "2002-02-27","email@umontreal.ca","4380001111"));

    }

    @DisplayName("Le prénom ne peut pas dépasser 50 caractères.")
    @Test
    public void prenomMauvaisFormatModi() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000001","Tester1",
                "Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1", "2002-02-27",
                "email@umontreal.ca","4380001111"));
    }

    @DisplayName("La date de naissance n'a pas été donnée dans le bon format.")
    @Test
    public void dateNaissanceMauvaisFormatModi() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000001","Tester1",
                "Test1", "20020227","email@umontreal.ca","4380001111"));
    }

    @DisplayName("L'adresse courriel n'a pas été donnée dans le bon format.")
    @Test
    public void emailMauvaisFormatModi() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000001","Tester1",
                "Test1", "2002-02-27","emailumontreal.ca","4380001111"));
    }

    @DisplayName("Le numéro de téléphone n'a pas été donné dans le bon format.")
    @Test
    public void numPhoneMauvaisFormatModi() {
        assertFalse(visiteurController.validationModifierVisiteur("000000000001","Tester1",
                "Test1", "2002-02-27","email@umontreal.ca","1111"));
    }

    @DisplayName("ajout d'un profil de vaccination vérification fonctionne")
    @Test
    public void ajoutProfilVaccinationTest() {
        assertTrue(visiteurController.validationProfilVaccination("000000000001", "2021-12-03",
                2, "Moderna", "1234567", "890"));
    }

    @DisplayName("Le numéro de compte n'est pas de la bonne longueur.")
    @Test
    public void numCompteMauvaisFormatProfilVacc() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "2021-11-11",
                2, "Moderna", "1234567", "890"));
    }

    @DisplayName("La date de vaccination n'est pas valide.")
    @Test
    public void dateVaccInvalide() {
        assertFalse(visiteurController.validationProfilVaccination("0000000000001", "2021-12-03",
                2, "Moderna", "1234567", "890"));

    }

    @DisplayName("La date de vaccination n'a pas été donnée dans le bon format.")
    @Test
    public void dateVaccMauvaisFormat() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "20211203",
                2, "Moderna", "1234567", "890"));
    }

    @DisplayName("Le type de dose est invalide.")
    @Test
    public void typeDoseInvalide() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "2021-12-03",
                3, "Moderna", "1234567", "890"));
    }

    @DisplayName("Le nom du vaccin rentré n'est pas parmi les choix possibles.")
    @Test
    public void numVaccinInvalide() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "2021-12-03",
                2, "abcndf", "1234567", "890"));
    }

    @DisplayName("Le code du vaccin rentré excède 24 caractères")
    @Test
    public void codeVaccinMauvaisFormat() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "2021-12-03",
                2, "Moderna", "1234567890123456789012345", "890"));
    }

    @DisplayName("Le lot du vaccin rentré excède 6 caractères")
    @Test
    public void lotVaccinMauvaisFormat() {
        assertFalse(visiteurController.validationProfilVaccination("000000000001", "2021-12-03",
                2, "Moderna", "1234567", "890123123"));
    }


}