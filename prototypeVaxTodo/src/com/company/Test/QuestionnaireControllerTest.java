import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QuestionnaireControllerTest {
    QuestionnaireController questionnaireController;
    Database database;
    Menu menu;
    Questionnaire questionnaire;
    CompteUtilisateur user;

    @BeforeEach
    protected void setup() throws ParseException {
        user = new CompteUtilisateur();
        database = new Database();
        menu = new Menu(user);
        questionnaire = new Questionnaire();

        Visiteur visiteurTest = new Visiteur("000000000000", "Tester", "Test", "1970-10-10", "email@umontreal.ca", "5143450000");
        database.addVisiteur(visiteurTest);

        questionnaireController = new QuestionnaireController(database, questionnaire, menu);
    }


    @DisplayName("Numéro de compte vérification fonctionne")
    @Test
    public void numeroCompteTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Numéro de compte vérification mauvaise longueur")
    @Test
    public void numeroCompteLess12Test() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("1", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Numéro de compte pas associé à un visiteur")
    @Test
    public void numeroCompteVisiteurInvalideTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000001", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Numéro assurance maladie vérifification fonctionne")
    @Test
    public void numeroAssuranceMaladieTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Numéro assurance maladie mauvais format")
    @Test
    public void numeroAssuranceMaladieFormatMauvaisTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST1234567@", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Date visite vérification fonctionne")
    @Test
    public void dateVisiteTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Date visite mauvaise entrée")
    @Test
    public void dateVisiteMauvaiseEntreeTest() {
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", "@", "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Date visite date avant")
    @Test
    public void dateVisiteAvantTest() {
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", "2021-12-17", "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Première dose = oui, fonctionne")
    @Test
    public void premiereDoseOuiTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Première dose = non, fonctionne")
    @Test
    public void premiereDoseNonTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "NON", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Première dose = autre entrée")
    @Test
    public void premiereDoseAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "@", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Contracte covid réponse = oui, fonctionne")
    @Test
    public void contracteCovidOuiTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Contracte covid réponse = non, fonctionne")
    @Test
    public void contracteCovidNonTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "NON","OUI","OUI", "Moderna"));
    }

    @DisplayName("Contracte covid réponse = autre entrée")
    @Test
    public void contracteCovidAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "@","OUI","OUI", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = oui, fonctionne")
    @Test
    public void symptomeCovidOuiTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = non, fonctionne")
    @Test
    public void symptomeCovidNonTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "NON","NON","OUI", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = autre entrée")
    @Test
    public void symptomeCovidAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","@","OUI", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = oui, fonctionne")
    @Test
    public void allergieOuiTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = non, fonctionne")
    @Test
    public void allergieNonTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "NON","OUI","NON", "Moderna"));
    }

    @DisplayName("Symptome covid réponse = autre entrée")
    @Test
    public void allergieAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","@", "Moderna"));
    }

    @DisplayName("Vaccin covid réponse = moderna, fonctionne")
    @Test
    public void vaccinModernaTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "moderna"));
    }

    @DisplayName("Vaccin covid réponse = pfizer, fonctionne")
    @Test
    public void vaccinPfizerTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "pfizer"));
    }

    @DisplayName("Vaccin covid réponse = astrazeneca, fonctionne")
    @Test
    public void vaccinAstraZenecaTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "astrazeneca"));
    }

    @DisplayName("Vaccin covid réponse = janssen, fonctionne")
    @Test
    public void vaccinJanssenTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertTrue(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "janssen"));
    }

    @DisplayName("Vaccin covid réponse = astrazeneca, fonctionne")
    @Test
    public void vaccinAutreTest() {
        Date now = Calendar.getInstance().getTime();
        String patternDate = "yyyy-MM-dd";
        assertFalse(questionnaireController.validationQuestionnaire("000000000000", "TEST12345678", new SimpleDateFormat(patternDate).format(now), "OUI", "OUI","OUI","OUI", "@"));
    }

    @DisplayName("Validation input pour get questionnaire = visiteur valid, fonctionne")
    @Test
    public void validationGetQuestionnaireTest() {
        assertTrue(questionnaireController.validationGetQuestionnaire("000000000000"));
    }

    @DisplayName("Validation input pour get questionnaire = visiteur invalide")
    @Test
    public void validationGetQuestionnaireInvalideTest() {
        assertFalse(questionnaireController.validationGetQuestionnaire(""));
    }
}